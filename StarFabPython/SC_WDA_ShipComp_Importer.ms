try (closeRolloutFloater WDAEquipMat) catch()

maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"
cache_path = getINISetting maxINI "SCTools" "cache_path"
script_path = getINISetting maxINI "SCTools" "script_path"
json_path = getINISetting maxINI "SCTools" "json_path"

rollout WDACompMat  "WDA Ship Component VRay Material Importer alpha 0.0.1" width:753 height:541
(
	
	editText 'MatTex_Pathway' "Texture Path" pos:[40,72] width:552 height:20 align:#left text: tex_path --SET THIS TO YOUR TEXTURE PATH
	editText 'TexMapWDA' "WDA File" pos:[56,112] width:350 height:20 align:#left text: tex_path --SET THIS TO YOUR TEXTURE PATH
	editText 'TextMapDDNA' "DDNA File" pos:[48,144] width:624 height:20 align:#left 
	groupBox 'BOX_TINT1' "TINT 1" pos:[16,184] width:232 height:198 align:#left
	groupBox 'BOXwear' "WEAR" pos:[264,184] width:232 height:198 align:#left
	groupBox 'BOXDirt' "DIRT" pos:[504,184] width:232 height:124 align:#left
	spinner 'Dirt_RGB_R' "Red" pos:[528,224] width:68 height:16 range:[0,1,1] type:#float scale:1e-10 align:#left
	spinner 'Dirt_RGB_G' "Green" pos:[520,248] width:67 height:16 range:[0,1,1] scale:1e-10  align:#left
	spinner 'Dirt_RGB_B' "Blue" pos:[528,272] width:67 height:16 range:[0,1,1] scale:1e-10 align:#left
	spinner 'Wear_RGB_R' "Red" pos:[288,224] width:68 height:16 range:[0,1,1] scale:1e-10 align:#left
	spinner 'Wear_RGB_G' "Green" pos:[280,248] width:67 height:16 range:[0,1,1] scale:1e-10 align:#left
	spinner 'Wear_RGB_B' "Blue" pos:[288,272] width:67 height:16 range:[0,1,1] scale:1e-10 align:#left
	spinner 'Tint_RGB_R' "Red" pos:[39,224] width:68 height:16 range:[0,1,1] type:#float scale:1e-10  align:#left
	spinner 'Tint_RGB_G' "Green" pos:[32,248] width:67 height:16 range:[0,1,1] scale:1e-10  align:#left
	spinner 'Tint_RGB_B' "Blue" pos:[40,272] width:67 height:16 range:[0,1,1] scale:1e-10 align:#left
	groupBox 'RGB_box_Albedo' "RGB" pos:[24,208] width:112 height:88 align:#left
	groupBox 'RGB_box_Wear' "RGB" pos:[272,208] width:112 height:88 align:#left
	groupBox 'RGB_box_Dirt' "RGB" pos:[512,208] width:112 height:88 align:#left
	groupBox 'Box_Gloss_Diff' "Gloss" pos:[144,208] width:88 height:88 align:#left
	groupBox 'Box_Gloss_Wear' "Gloss" pos:[392,208] width:88 height:88 align:#left
	groupBox 'Box_Gloss_Dirt' "Gloss" pos:[632,208] width:88 height:88 align:#left
	groupBox 'BOX_Tile_Albedo' "Tile Scale" pos:[24,304] width:90 height:40 align:#left
	groupBox 'BOX_Tile_Wear' "Tile Scale" pos:[272,304] width:90 height:40 align:#left
	spinner 'Gloss_Tint1' "" pos:[160,240] width:64 height:16 range:[0,1,1] scale:1e-10 align:#left
	spinner 'Gloss_Wear' "" pos:[400,240] width:64 height:16 range:[0,1,1] scale:1e-10 align:#left
	spinner 'Gloss_Mult_Dirt' "" pos:[648,240] width:64 height:16 range:[0,1,1] scale:1e-10 align:#left
	bitmap 'bmp1' "Bitmap" pos:[192,8] width:384 height:50 enabled:true fileName:"WDA_Equipment.jpg" align:#left
	bitmap 'bmp2' "Bitmap" pos:[648,17] width:80 height:80 fileName:"Logo_Community_logo.tiff" align:#left
	---tiling 
	spinner 'OG_Tile_Tint1' "" pos:[34,320] width:72 height:16 range:[0,1000,8] type:#float scale:0.001 align:#left
	spinner 'OG_Tile_Wear' "" pos:[278,320] width:72 height:16 range:[0,1000,8] type:#float scale:0.001 align:#left
	spinner 'Tile_Tint1' "Tile Layer" pos:[515,330] width:50 height:16 range:[0,1000,1] type:#float scale:0.001 align:#left
	spinner 'Tile_Wear' "Tile Wear" pos:[515,350] width:50 height:16 range:[0,1000,1] type:#float scale:0.001 align:#left
	spinner 'Gloss_Mult_Tint1' "Gloss Layer" pos:[625,330] width:50 height:16 range:[0,1,1] type:#float scale:0.001 align:#left
	spinner 'Gloss_Mult_Wear' "Gloss Wear" pos:[625,350] width:50 height:16 range:[0,1,1] type:#float scale:0.001 align:#left
	
	button 'Btn_Make' "Load WDA Mat" pos:[500,470] width:240 height:47 align:#left
	
	editText 'MatName_Tint1' "Tint 1 Mat Name" pos:[150,391] width:366 height:20 align:#left
	editText 'MatName_Wear' "Wear Mat Name" pos:[150,423] width:366 height:20 align:#left text: "highresolution/metal/weapon_bare_01.mtl"
	editText 'WDA_Name' "Mat Name" pos:[220,505] width:220 height:20 align:#left
	editText 'MatLibPath' "MatLib Path" pos:[89,451] width:387 height:21 align:#left text:tex_path --SET THIS TO YOUR MAT PATH
	editText 'MTL_Name' "MTL Name:" pos:[20,505] width:190 height:20 align:#left
	
	checkbox 'CK_Gloss_Base' "Apply" pos:[160,260] width:64 height:16 checked:off 
	checkbox 'CK_Gloss_Wear' "Apply" pos:[402,260] width:64 height:16 checked:off 
	checkbox 'CK_Gloss_Dirt' "Apply" pos:[652,260] width:64 height:16 checked:off 
	
	checkbox 'Diff_apply_base' "Apply" pos:[65,195] width:64 height:16 checked:on 
	checkbox 'Diff_apply_wear' "Apply" pos:[315,195] width:64 height:16 checked:on 
	
	checkbox 'MetalL' "Metal" pos:[530,391] width:64 height:16 checked:off 
	checkbox 'MetalW' "Metal" pos:[530,423] width:64 height:16 checked:off 
	
	groupBox 'BOX_gammalayer2' "Gamma" pos:[116,304] width:115 height:75 align:#left
	groupBox 'BOX_gammawear' "Gamma" pos:[365,304] width:110 height:75 align:#left
	spinner 'G_Tint1' "" pos:[134,320] width:72 height:16 range:[0,10,2.2] type:#float scale:0.001 align:#left
	spinner 'G_Wear' "" pos:[380,320] width:72 height:16 range:[0,10,2.2] type:#float scale:0.001 align:#left
	checkbox 'G_applyT' "Apply" pos:[145,350] width:64 height:16 checked:off
	checkbox 'G_applyW' "Apply" pos:[395,350] width:64 height:16 checked:off
	button 'WDABrowse' "Browse" pos:[410,112] width:80 height:20 align:#left 
	groupBox 'BOsXDirt' "Scale" pos:[504,310] width:232 height:70 align:#left
	spinner 'mtl_select_spinner' "MTL ID" pos:[20,25] width:50 height:18  align:#left range:[0,100,3] scale:1 type:#integer 
	button 'mtl_load' "Load" pos:[30,44] width:80 height:18 align:#left enabled:true
	
		on mtl_load pressed do
	(
		Import_Instruction= getOpenFileName caption:"Pick MTL:" types:"CryEngine MTL in JSON (*.mtl)|*.mtl|""All those other Files|*.*"
		
		MTL_Name.text=getFilenameFile Import_Instruction
		Mid_File=createfile "T:/SC_Cache/WS2m.txt" mode:"wt" --SET THIS TO TEMP LOCATION
		print (mtl_select_spinner.value) to: Mid_File
		close Mid_File
		
		MLTGet_Maker=openfile "T:/SC_Cache/WSGetMtl.txt" mode:"wt" --SET THIS TO TEMP LOCATION
		print (Import_Instruction) to: MLTGet_Maker
		close MLTGet_Maker
		

		MTL_Name.text=getFilenameFile Import_Instruction


		try python.ExecuteFile script_path + "SC_MTL_JSON_WS.py" catch (messageBox "Invalid Material")                --SET THIS TO YOUR SCRIPT LOCATION
		deleteFile cache_path + "WE2m.txt"
		
		mtl_in_file = openFile "T://SC_Cache//WSOutput.txt" mode: "rt" --SET THIS TO TEMP LOCATION
		
		mtlname=execute(readline mtl_in_file)
		Base_TexMap_DDNA=execute(readline mtl_in_file)
		Base_TexMap_WDA=execute(readline mtl_in_file)
		DirtR=execute(readline mtl_in_file)
		DirtG=execute(readline mtl_in_file)
		DirtB=execute(readline mtl_in_file)
		BL1_TintR=execute(readline mtl_in_file)
		BL1_TintG=execute(readline mtl_in_file)
		BL1_TintB=execute(readline mtl_in_file)
		WL1_TintR=execute(readline mtl_in_file)
		WL1_TintG=execute(readline mtl_in_file)
		WL1_TintB=execute(readline mtl_in_file)
		BL1_Gloss=execute(readline mtl_in_file)
		WL1_Gloss=execute(readline mtl_in_file)
		BL1_UVTiling=execute(readline mtl_in_file)
		WL1_UVTiling=execute(readline mtl_in_file)
		BL1_Path=execute(readline mtl_in_file)
		WL1_Path=execute(readline mtl_in_file)
		
		WDA_Name.text=mtlname
		TextMapDDNA.text=Base_TexMap_DDNA
		TexMapWDA.text=(MatTex_Pathway.text+Base_TexMap_WDA)
		Dirt_RGB_R.value=DirtR as float
		Dirt_RGB_G.value=DirtG as float
		Dirt_RGB_B.value=DirtB as float
		Tint_RGB_R.value=BL1_TintR as float
		Tint_RGB_G.value=BL1_TintG as float
		Tint_RGB_B.value=BL1_TintB as float
		Wear_RGB_R.value=WL1_TintR as float
		Wear_RGB_G.value=WL1_TintG as float
		Wear_RGB_B.value=WL1_TintB as float
		Gloss_Tint1.value=BL1_Gloss as float
		Gloss_Wear.value=WL1_Gloss as float
		Tile_Tint1.value=BL1_UVTiling as float
		Tile_Wear.value=WL1_UVTiling as float
		MatName_Tint1.text=BL1_Path
		MatName_Wear.text=WL1_Path
		

		close mtl_in_file

	)
	
	
	
	on WDABrowse pressed do
	(
		TexMapWDA.text=getOpenFileName initialDir:(MatTex_Pathway.text) caption:"Select WDA File:" types:"All Files(*.*)|*.*|All Picture Files(*.TIF,*.tif, *.tiff, *.png, *.tga, *.bmp, *.jpeg, *.jpg, *.dds)|*.TIF,*.tif, *.tiff, *.png, *.tga, *.bmp, *.jpeg, *.jpg, *.dds|*.tif,*.tiff(*.tif,*.tiff)|*.tif,*.tiff,*.TIF|*.png(*.png)|*.png|*.tga(*.tga)|*.tga" 		
	)
	
	
	on Btn_Make pressed do
	(
		B2_MatNameT=substituteString MatName_Tint1.text "materials/layers/" ""
		B1_MatNameT=substituteString MatName_Wear.text "materials/layers/" ""
		
		LayerMatFileName=substituteString B2_MatNameT "/" "_"
		WearMatFileName=substituteString B1_MatNameT "/" "_"
		

		--create materials
		meditMaterials[activeMeditSlot]=VRayBlendMtl()
		meditMaterials[activeMeditSlot].name=(MTL_Name.text+"_mtl_"+WDA_Name.text)
		if MatName_Tint1.text !="" do
		(
			TintLib=loadTempMaterialLibrary(MatLibPath.text)
			meditMaterials[activeMeditSlot].baseMtl=TintLib[LayerMatFileName]
		)
		--Load Layers
		if MatName_Wear.text !="" do
		(
			TintLib=loadTempMaterialLibrary(MatLibPath.text)
			meditMaterials[activeMeditSlot].coatMtl[1]=TintLib[WearMatFileName]
		)

		--Dirt Layer
		if TextMapDDNA.text != "" do
		(
			meditMaterials[activeMeditSlot].coatMtl[2]=VRayMtl()
			meditMaterials[activeMeditSlot].coatMtl[2].name="WDA Dirt"
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse=CompositeMap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].color=color (Dirt_RGB_R.value*255) (Dirt_RGB_G.value*255) (Dirt_RGB_B.value*255) 255
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].name="DIRTCOLOR"
			meditMaterials[activeMeditSlot].coatMtl[2].reflection=color 0 0 0
			meditMaterials[activeMeditSlot].coatMtl[2].reflection_glossiness= (Gloss_Mult_Dirt.value*100)
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump=VrayNormalMap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map=VRayBitmap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.HDRIMapName=(MatTex_Pathway.text+TextMapDDNA.text)
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump.normal_map.color_space=0
		)
		
		if TextMapDDNA.text == "" do
		(
			meditMaterials[activeMeditSlot].coatMtl[2]=VRayMtl()
			meditMaterials[activeMeditSlot].coatMtl[2].name="WDA Dirt"
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse=CompositeMap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].color=color (Dirt_RGB_R.value*255) (Dirt_RGB_G.value*255) (Dirt_RGB_B.value*255) 255
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[1].name="DIRTCOLOR"
			meditMaterials[activeMeditSlot].coatMtl[2].reflection=color 0 0 0
			meditMaterials[activeMeditSlot].coatMtl[2].reflection_glossiness= Gloss_Mult_Dirt.value
		)
		
		--Load Tint1 para


		if MetalL.state==true do
		(

				meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2].color=color (Tint_RGB_R.value*255)(Tint_RGB_G.value*255)(Tint_RGB_B.value*255) 255
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2].lightnessMode = 1
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.blendMode[2]=5 
				if G_applyT.state==true do
				(
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2].gammaRGB=G_Tint1.value
				)
		)
		
		if MetalW.state==true do
		(
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[2].color=color (Tint_RGB_R.value*255)(Tint_RGB_G.value*255)(Tint_RGB_B.value*255) 255
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.maplist[2].lightnessMode = 1
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflection.blendMode[2]=5 
				if G_applyW.state==true do
				(
				meditMaterials[activeMeditSlot].baseMtl.texmap_reflection.maplist[2].gammaRGB=G_Wear.value
				)
		)
		

		
		
		if iskindof meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness CompositeTexturemap do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.opacity[1]=Gloss_Mult_Tint1.value*100
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].opacity[1]=Gloss_Tint1.value*100
		)
		
		if meditMaterials[activeMeditSlot].baseMtl.texmap_bump !=VRayNormalMap do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump=VRayNormalMap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map_multiplier=1.0 
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump_multiplier=100
		)
		if iskindof meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map Bitmaptexture do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map=meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map_multiplier =meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map_multiplier
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map_multiplier=1.0 
		)
		if iskindof meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map VRayBitmap do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map=meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map_multiplier =meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map_multiplier
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map_multiplier=1.0 
		)
		
		if iskindof meditMaterials[activeMeditSlot].baseMtl.texmap_bump VRayNormalMap do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump_multiplier=100
		)

		if TextMapDDNA.text !="" do
		(
			TNormString=TextMapDDNA.text
			TextMapDDNA_gloss=substring TNormString 1 (TNormString.count-4)+"_glossMap.tif" 
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2]=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].HDRIMapName=(MatTex_Pathway.text+TextMapDDNA_gloss)
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].maplist[2].color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].blendMode[2]=5
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.HDRIMapName=(MatTex_Pathway.text+TextMapDDNA.text)	
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map.color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump_multiplier = 100
		)
		
			
		--tile stuff vra
		if iskindof meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mapList[1].maplist[1] VRayBitmap do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mapList[1].mapList[1].coords.U_Tiling = OG_Tile_Tint1.value*Tile_Tint1.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.mapList[1].mapList[1].coords.V_Tiling = OG_Tile_Tint1.value*Tile_Tint1.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.mapList[1].mapList[1] VRayBitmap do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].mapList[1].coords.U_Tiling = OG_Tile_Tint1.value*Tile_Tint1.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_reflectionGlossiness.maplist[1].mapList[1].coords.V_Tiling = OG_Tile_Tint1.value*Tile_Tint1.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map VRayBitmap do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.coords.U_Tiling = OG_Tile_Tint1.value*Tile_Tint1.value
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump.bump_map.coords.V_Tiling = OG_Tile_Tint1.value*Tile_Tint1.value
		)
		--Load Wear para

		
		
		if iskindof meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness CompositeTexturemap do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.opacity[1]=Gloss_Mult_Wear.value*100
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].opacity[1]=Gloss_Wear.value*100
		)
	
		
		if meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump !=VRayNormalMap do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump=VRayNormalMap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map_multiplier=1.0 
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump_multiplier=100
		)
		
		
		if iskindof meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map VRayBitmap do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map=meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map_multiplier =meditMaterials[activeMeditSlot].baseMtl.texmap_bump.normal_map_multiplier
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map_multiplier=1.0 
		)
		if iskindof meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump VRayNormalMap do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump_multiplier=100
		)

		if TextMapDDNA.text !="" do
		(
			TNormString=TextMapDDNA.text
			TextMapDDNA_gloss=substring TNormString 1 (TNormString.count-4)+"_glossMap.tif" 
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[2]=VRayBitmap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].maplist[2].HDRIMapName=(MatTex_Pathway.text+TextMapDDNA_gloss)
			meditMaterials[activeMeditSlot].coatMtl[1]texmap_reflectionGlossiness.maplist[1].maplist[2].color_space=0
			meditMaterials[activeMeditSlot].coatMtl[1]texmap_reflectionGlossiness.blendMode[2]=5
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map=VRayBitmap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.HDRIMapName=(MatTex_Pathway.text+TextMapDDNA.text)	
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.normal_map.color_space=0
		)
		
			
		--tile stuff

		--tile v stuff
		if iskindof meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mapList[1].mapList[1] VRayBitmap do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mapList[1].mapList[1].coords.U_Tiling = OG_Tile_Wear.Value *Tile_Wear.value
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.mapList[1].mapList[1].coords.V_Tiling = OG_Tile_Wear.value *Tile_Wear.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.mapList[1] VRayBitmap do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].mapList[1].coords.U_Tiling = OG_Tile_Wear.value *Tile_Wear.value
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_reflectionGlossiness.maplist[1].mapList[1].coords.V_Tiling = OG_Tile_Wear.value *Tile_Wear.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map VRayBitmap do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map.coords.U_Tiling = OG_Tile_Wear.value*Tile_Wear.value
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump.bump_map.coords.V_Tiling = OG_Tile_Wear.value*Tile_Wear.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].coatMtl[1] VRayMtl do
		(
			meditMaterials[activeMeditSlot].coatMtl[1].name="WDA WEAR EQUIPMENT"
		)
		if iskindof meditMaterials[activeMeditSlot].baseMtl VRayMtl do
		(
			(meditMaterials[activeMeditSlot].baseMtl).name="WDA TINT1 EQUIPMENT"
		)
		if iskindof meditMaterials[activeMeditSlot].coatMtl[2] VRayMtl do
		(
			meditMaterials[activeMeditSlot].coatMtl[2].name="WDA DIRT EQUIPMENT"

		--WDA masking 
		if TexMapWDA.text !="" do
		(

			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1].rewireR=10
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1].rewireB=2
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1].map.HDRIMapName=(TexMapWDA.text)
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].blendMode[1]=0
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[4].blendMode[2]=23
			meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[4]=5
			--wear

			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4]=CompositeTexturemap ()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1].rewireR=10
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1].rewireB=2
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1].map.HDRIMapName=(TexMapWDA.text)
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].blendMode[1]=0
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[4].blendMode[2]=23
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[4]=5
			--wearmask
			meditMaterials[activeMeditSlot].texmap_blend[1]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].rewireR=4
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].rewireG=5
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].rewireB=6
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].rewireA=3
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1].rewireR=0
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1].rewireB=10
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1].map.HDRIMapName=(TexMapWDA.text)
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.blendMode[1]=0
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].map.blendMode[2]=23
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].name="Wear_CONTROL"
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[2].color=color 0 0 0 255
			meditMaterials[activeMeditSlot].texmap_blend[1].blendMode[2]=5
			meditMaterials[activeMeditSlot].Blend[1]= color 0 0 0 			
 			--dirtmask
			meditMaterials[activeMeditSlot].texmap_blend[2]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].rewireR=4
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].rewireG=5
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].rewireB=6
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].rewireA=3
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1].rewireR=0
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1].rewireB=10
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1].map.HDRIMapName=(TexMapWDA.text)
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.blendMode[1]=0
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].map.blendMode[2]=23
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[2].name="DIRT_CONTROL"
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[2].color=color 0 0 0 255
			meditMaterials[activeMeditSlot].texmap_blend[2].blendMode[2]=5
			meditMaterials[activeMeditSlot].Blend[2]= color 0 0 0 
			--dirt albedo
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1].rewireR=10
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1].rewireB=2
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1].map.HDRIMapName=(TexMapWDA.text)
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].blendMode[1]=0
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.maplist[3].blendMode[2]=23
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_diffuse.blendMode[3]=5

		)
		if iskindof meditMaterials[activeMeditSlot] VRayBlendMtl do
		(
			meditMaterials[activeMeditSlot].baseMtl.texmap_bump_multiplier = 100
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_bump_multiplier = 100
			meditMaterials[activeMeditSlot].coatMtl[2].texmap_bump_multiplier = 100
		)
		
		if TexMapWDA.text =="" do
		(
			meditMaterials[activeMeditSlot].texmap_blend[1]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].name="Wear_CONTROL"
			meditMaterials[activeMeditSlot].texmap_blend[1].maplist[1].color=color 0 0 0 255
			
			meditMaterials[activeMeditSlot].texmap_blend[2]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].name="DIRT_CONTROL"
			meditMaterials[activeMeditSlot].texmap_blend[2].maplist[1].color=color 0 0 0 255
			
			
		)
		
		
		if TexMapWDA.text =="" do
		(
			meditMaterials[activeMeditSlot].Blend[1]= color 0 0 0 
			meditMaterials[activeMeditSlot].Blend[2]= color 0 0 0 
		)
		
		if Diff_apply_wear.state==true do
			(
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[2].color=color (Wear_RGB_R.value*255)(Wear_RGB_G.value*255)(Wear_RGB_B.value*255) 255
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[2]=5 
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[2].lightnessMode = 1
			meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.blendMode[2]=5 
			if G_applyW.state==true do
				(
				meditMaterials[activeMeditSlot].coatMtl[1].texmap_diffuse.maplist[2].gammaRGB=G_Wear.value
				)	
				
			)
			
		if  Diff_apply_base.state==true do
			(
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2]=ColorCorrection()
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].color=color (Tint_RGB_R.value*255)(Tint_RGB_G.value*255)(Tint_RGB_B.value*255) 255
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].lightnessMode = 1
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.blendMode[2]=5 
			
		if G_applyT.state==true do
				(
				meditMaterials[activeMeditSlot].baseMtl.texmap_diffuse.maplist[2].gammaRGB=G_Tint1.value
				)
			
			)
		)
		
	)
)
	

createDialog WDACompMat 
