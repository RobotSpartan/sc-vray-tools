try (closeRolloutFloater POMLoader) catch()

maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"
cache_path = getINISetting maxINI "SCTools" "cache_path"
script_path = getINISetting maxINI "SCTools" "script_path"

rollout POMLoader "Parallax Occlusion Map Loader alpha 0.0.25" width:884 height:530
(

	
	bitmap 'bmp1' "Bitmap" pos:[55,14] width:768 height:84 fileName:"POM_sheet_loader.tiff" align:#left
	editText 'NameEnter' "Name" pos:[40,123] width:375 height:20 align:#left
	editText 'TexMapPath' "Texture Path" pos:[446,112] width:397 height:24 align:#left text: tex_path --SET THIS TO YOUR TEXTURE PATH
	groupBox 'grp1' "Diffuse [1.0]" pos:[13,150] width:295 height:86 align:#left
	groupBox 'grp2' "Specular [1.0]" pos:[323,153] width:295 height:86 align:#left
	groupBox 'grp3' "Gloss [255]" pos:[630,155] width:92 height:86 align:#left
	editText 'TexMap_DDNA' "DDNA [TextSlot2]" pos:[24,296] width:634 height:20 align:#left
	editText 'TexMap_Diff' "Diff [TextSlot1]" pos:[22,261] width:637 height:20 align:#left
	editText 'TexMap_SPEC' "Spec [TextSlot4]" pos:[31,331] width:627 height:20 align:#left
	editText 'TexMap_DISPL' "DISPL [TextSlot8]" pos:[26,404] width:633 height:20 align:#left
	checkbox 'chk1_opac' "Opacity" pos:[688,257] width:60 height:15 align:#left
	editText 'TexMap_Detail' "Detail [TextSlot6]" pos:[31,368] width:510 height:20 align:#left
	spinner 'Diff_R' "R" pos:[26,189] width:73 height:16 range:[0,1,1] type:#float scale:1e-15 align:#left
	spinner 'Diff_G' "G" pos:[118,189] width:73 height:16 range:[0,1,1] scale:1e-15 align:#left
	spinner 'Diff_B' "B" pos:[214,190] width:74 height:16 range:[0,1,1] scale:1e-15 align:#left
	spinner 'Spec_R' "R" pos:[333,189] width:73 height:16 range:[0,1,1] type:#float scale:1e-15 align:#left
	spinner 'Spec_G' "G" pos:[425,189] width:73 height:16 range:[0,1,1] scale:1e-15 align:#left
	spinner 'Spec_B' "B" pos:[521,190] width:74 height:16 range:[0,1,1] scale:1e-15 align:#left
	spinner 'GlossMulti' "" pos:[641,187] width:75 height:16 range:[0,255,255] scale:1  align:#left
	bitmap 'bmp2' "Bitmap" pos:[758,160] width:80 height:80 fileName:"Logo_Community_logo.tiff" align:#left
	spinner 'POMdisp' "PomDisplacement" pos:[27,461] width:77 height:16 range:[0,10,0.025] scale:1e-07  align:#left
	groupBox 'grp5' "Detail Map Parameters" pos:[194,441] width:433 height:74 align:#left
	button 'btn_make' "Import Material" pos:[688,467] width:175 height:48 align:#left
	spinner 'UTile' "U Tile" pos:[289,460] width:79 height:16 range:[0,100,1] scale:0.001  align:#left
	spinner 'VTile' "V Tile" pos:[289,481] width:80 height:16 range:[0,100,1] scale:0.001  align:#left
	spinner 'Uoffset' "U Offset     " pos:[403,461] width:92 height:16 range:[0,100,0] scale:0.001  align:#left
	spinner 'Voffset' "V Offset     " pos:[403,483] width:93 height:16 range:[0,100,0] scale:0.001  align:#left
	groupBox 'grp6' "Detail Scale" pos:[664,348] width:199 height:117 align:#left
	spinner 'Scale_Gloss' "Gloss" pos:[700,397] width:76 height:16 range:[0,100,.5] align:#left
	spinner 'Scale_Diff' "Diff" pos:[708,368] width:77 height:16 range:[0,100,.5] align:#left
	spinner 'Scale_ddna' "DDNA" pos:[694,426] width:74 height:16 range:[0,100,.5] align:#left
	checkbox 'chk3_decal' "POM DECAL" pos:[761,257] width:77 height:15 align:#left
	checkbox 'chk6_CCmap' "CC map" pos:[554,375] width:95 height:18 align:#left checked:true
	editText 'MTL_Name' "MTL Name" pos:[14,100] width:402 height:20 align:#left
	checkbox 'applygloss' "Apply Gloss" pos:[664,296] width:95 height:18 align:#left checked:on
	
	on btn_make pressed do
	(	
			if chk3_decal.state==false do
			(
				if chk1_opac.state==true do
				(
					DecalLib=loadTempMaterialLibrary(script_path + "SCM.mat")--SET to your 3DS Max Directory
					meditMaterials[activeMeditSlot]=DecalLib["POMLoadOpac"]
				)
				
				if chk1_opac.state==false do
				(
					DecalLib=loadTempMaterialLibrary(script_path + "SCM.mat")--SET to your 3DS Max Directory
					meditMaterials[activeMeditSlot]=DecalLib["POMLoad"]
				)
				if NameEnter.text!="" do
				(
					meditMaterials[activeMeditSlot].name=(MTL_Name.text+"_mtl_"+NameEnter.text)
					meditMaterials[activeMeditSlot].diffuse=color (Diff_R.value*255)(Diff_G.value*255)(Diff_B.value*255)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=colorcorrection()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].color=color (Diff_R.value*255)(Diff_G.value*255)(Diff_B.value*255) 255
					meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[3]=5
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[2]=colorcorrection()
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].color=color (Spec_R.value*255)(Spec_G.value*255)(Spec_B.value*255) 255
					meditMaterials[activeMeditSlot].Reflection=color (Spec_R.value*255)(Spec_G.value*255)(Spec_B.value*255)
					meditMaterials[activeMeditSlot].texmap_reflection.blendMode[2]=15
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.opacity[1]=(GlossMulti.value*100)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.blendMode[3]=5
					meditMaterials[activeMeditSlot].option_opacityMode = 0
					meditMaterials[activeMeditSlot].texmap_bump_multiplier = 100
					
					meditMaterials[activeMeditSlot].texmap_diffuse.mapEnabled[1] = off
					meditMaterials[activeMeditSlot].texmap_reflection.mapEnabled[1] = off
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.mapEnabled[1] = off
					meditMaterials[activeMeditSlot].texmap_bump.normal_map_on = off

				)
				if TexMap_DDNA.text=="" do
				(
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[3]=colorcorrection()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[3].color=color 255 255 255 255
				)
				if TexMap_Diff.text!="" do
				(
					meditMaterials[activeMeditSlot].texmap_diffuse.mapEnabled[1] = on
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].sourceMap.filename =(TexMapPath.text+TexMap_Diff.text)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].sourceMap.autogamma = 1
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].sourceMap.Pos_map.heightTexMap = (TexMapPath.text+TexMap_DISPL.text)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].sourceMap.Pos_map.depth_mult = (POMdisp.value*100)
					
				)
				if TexMap_SPEC.text!="" do
				(
					meditMaterials[activeMeditSlot].texmap_reflection.mapEnabled[1] = on
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].sourceMap.filename =(TexMapPath.text+TexMap_SPEC.text)
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].sourceMap.autogamma = 0
				)
				if TexMap_DDNA.text!="" do
				(
					BNormString=TexMap_DDNA.text
					Bl_GlossPath=substring BNormString 1 (BNormString.count-4)+"_glossMap.tif"

					meditMaterials[activeMeditSlot].texmap_bump.normal_map_on = on
					meditMaterials[activeMeditSlot].texmap_bump.normal_map.sourceMap.filename=(TexMapPath.text+TexMap_DDNA.text)
					meditMaterials[activeMeditSlot].texmap_bump.normal_map.sourceMap.autogamma = 0
				)
				
				if TexMap_DDNA.text!="" and applygloss.state==on do
				(
				meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.mapEnabled[1] = on
				meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.filename =(TexMapPath.text+Bl_GlossPath)
				meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.autogamma = 0
				)
				if TexMap_Detail.text !="" and chk6_CCmap.state==true do
				(
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=CompositeTextureMap()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1]=ColorCorrection()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireR=0
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireG=10
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireB=10
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireA=9
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.color_space=0
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].blendMode[1]=0
					meditMaterials[activeMeditSlot].texmap_diffuse.opacity[2]=(Scale_Diff.value *100)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[2]=RGB_Tint()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].blendMode[2]=23
					meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[2]=5
					
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2]=CompositeTextureMap()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1]=ColorCorrection()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireR=10
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireG=10
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireB=0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireA=9
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.color_space=0.0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[1]=0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].opacity[2]=(Scale_Gloss.value *100)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[2]=RGB_Tint()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[2]=14
					
					meditMaterials[activeMeditSlot].texmap_bump.bump_map=CompositeTextureMap()
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1]=ColorCorrection()
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireR=1
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireG=3
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireB=9
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireA=3
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.color_space=0
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.alphaSource=0
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[2]=RGB_Tint()
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.blendMode[2]=23
					meditMaterials[activeMeditSlot].texmap_bump.bump_map_multiplier=Scale_ddna.value
					
				)
				
				if TexMap_Detail.text !="" and chk6_CCmap.state==false do
				(
					Bl_DetailString=TextMap_Detail.text
					SDetailNorm=substring Bl_DetailString 1 (Bl_DetailString.Count-7)+"dna.tif"
					SDetailGloss=substring Bl_DetailString 1 (Bl_DetailString.Count-7)+"_glossMap.tif"
					
					meditMaterials[activeMeditSlot].texmap_bump=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_bump.HDRIMapName=(TexMapPath.text+SDetailNorm)
					meditMaterials[activeMeditSlot].texmap_bump.color_space=0.0
					meditMaterials[activeMeditSlot].texmap_bump.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_bump.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_bump.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_bump.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map_multiplier=DetailScale_Normal.value
					
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2]=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].HDRIMapName=(TexMapPath.text+SDetailGloss)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].color_space=0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].opacity=(DetailScale_Gloss.value *100)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.U_Tiling =  UTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].opacity[2]=(Scale_Gloss.value *100)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[2]=14
					
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].color_space=0.0
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].opacity=(DetailScale_Diff.value *100)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].coords.U_Offset = DOffsetU.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].coords.V_Offset = DOffsetV.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].coords.U_Tiling = DTileU.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].coords.V_Tiling = DTileV.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].opacity=(Scale_Diff.value *100)
					meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[2]=5

					
				)
			)	
	
			if chk3_decal.state==true do
			(
			DecalLib=loadTempMaterialLibrary(script_path + "SCM.mat")--SET to your 3DS Max Directory
			meditMaterials[activeMeditSlot]=DecalLib["POMDecal"]
			meditMaterials[activeMeditSlot].refraction_ior=1.45
			

				if NameEnter.text!="" do
				(
					meditMaterials[activeMeditSlot].name=(MTL_Name.text+"_mtl_"+NameEnter.text)
					meditMaterials[activeMeditSlot].diffuse=color 46 46 46
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=colorcorrection()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].color=color 46 46 46 255
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].lightnessMode=1
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].gammaRGB = 0.45
					meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[1]=0
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=colorcorrection()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].color=color 128 128 128 255
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].lightnessMode=1
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].gammaRGB = 0.45
					meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[2]=5
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[3]=colorcorrection()
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[3].color=color (Spec_R.value*255)(Spec_G.value*255)(Spec_B.value*255) 255
					meditMaterials[activeMeditSlot].texmap_reflection.blendMode[3]=5
					meditMaterials[activeMeditSlot].texmap_reflection.opacity[3]=50
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.opacity[1]=((GlossMulti.value/255)*100)
					meditMaterials[activeMeditSlot].reflection_glossiness=(GlossMulti.value/255)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.blendMode[3]=5
					meditMaterials[activeMeditSlot].option_opacityMode = 0
					meditMaterials[activeMeditSlot].texmap_bump_multiplier = 100
					
					meditMaterials[activeMeditSlot].texmap_diffuse.mapEnabled[1] = on
					meditMaterials[activeMeditSlot].texmap_reflection.mapEnabled[2] = off
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.mapEnabled[1] = off
					meditMaterials[activeMeditSlot].texmap_bump.normal_map_on = off
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.Pos_map.heightTexMap = (TexMapPath.text+TexMap_DISPL.text)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.Pos_map.depth_mult = (POMdisp.value*100)	
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.Pos_map.minLayers = 16					
					)
				if TexMap_DDNA.text=="" do
				(
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[3]=colorcorrection()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[3].color=color 255 255 255 255
				)
				

					
				
				if TexMap_SPEC.text!="" do
				(
					meditMaterials[activeMeditSlot].texmap_reflection.mapEnabled[2] = on
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].sourceMap.filename =(TexMapPath.text+TexMap_SPEC.text)
					meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].sourceMap.autogamma = 0
					meditMaterials[activeMeditSlot].texmap_reflection.blendmode[2]=5
				)
				if TexMap_DDNA.text!="" do
				(
					BNormString=TexMap_DDNA.text
					Bl_GlossPath=substring BNormString 1 (BNormString.count-4)+"_glossMap.tif"
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.mapEnabled[1] = on
					meditMaterials[activeMeditSlot].texmap_bump.normal_map_on = on
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.filename =(TexMapPath.text+Bl_GlossPath)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.autogamma = 0
					meditMaterials[activeMeditSlot].texmap_bump.normal_map.sourceMap.filename=(TexMapPath.text+TexMap_DDNA.text)
					meditMaterials[activeMeditSlot].texmap_bump.normal_map.sourceMap.autogamma = 0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.Pos_map.heightTexMap = (TexMapPath.text+TexMap_DISPL.text)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.Pos_map.depth_mult = (POMdisp.value*100)	
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].sourceMap.Pos_map.minLayers = 16					
				)
				if TexMap_Detail.text !="" and chk6_CCmap.state==true do
				(
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=CompositeTextureMap()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1]=ColorCorrection()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].rewireR=0
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].rewireG=10
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].rewireB=10
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].rewireA=9
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].map=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].map.HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].map.color_space=0
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].map.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].map.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].map.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[1].map.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].blendMode[1]=0
					meditMaterials[activeMeditSlot].texmap_diffuse.opacity[3]=(Scale_Diff.value *100)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].maplist[2]=RGB_Tint()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].blendMode[2]=23
					meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[2]=5
					
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2]=CompositeTextureMap()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1]=ColorCorrection()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireR=10
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireG=10
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireB=0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireA=9
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.color_space=0.0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[1]=0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].opacity=(Scale_Gloss.value*100)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[2]=RGB_Tint()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[2]=14
					
					meditMaterials[activeMeditSlot].texmap_bump.bump_map=CompositeTextureMap()
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1]=ColorCorrection()
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireR=1
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireG=3
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireB=9
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireA=3
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.color_space=0
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.alphaSource=0
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_bump.maplist[2].maplist[2]=RGB_Tint()
					meditMaterials[activeMeditSlot].texmap_bump.blendMode[2]=23
					meditMaterials[activeMeditSlot].texmap_bump.bump_map_multiplier=Scale_ddna.value
					
				)
				
				if TexMap_Detail.text !="" and chk6_CCmap.state==false do
				(
					Bl_DetailString=TextMap_Detail.text
					SDetailNorm=substring Bl_DetailString 1 (Bl_DetailString.Count-7)+"dna.tif"
					SDetailGloss=substring Bl_DetailString 1 (Bl_DetailString.Count-7)+"_glossMap.tif"
					
					meditMaterials[activeMeditSlot].texmap_bump=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_bump.HDRIMapName=(TexMapPath.text+SDetailNorm)
					meditMaterials[activeMeditSlot].texmap_bump.color_space=0.0
					meditMaterials[activeMeditSlot].texmap_bump.coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_bump.coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_bump.coords.U_Tiling = UTile.value
					meditMaterials[activeMeditSlot].texmap_bump.coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_bump.bump_map_multiplier=DetailScale_Normal.value
					
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2]=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].HDRIMapName=(TexMapPath.text+SDetailGloss)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].color_space=0
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].opacity=(DetailScale_Gloss.value *100)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.U_Offset = Uoffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.V_Offset = Voffset.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.U_Tiling =  UTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.V_Tiling = VTile.value
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].opacity[2]=(Scale_Gloss.value *100)
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[2]=14
					
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=VRayBitmap()
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].HDRIMapName=(TexMapPath.text+TexMap_Detail.text)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].color_space=0.0
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].opacity=(DetailScale_Diff.value *100)
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].coords.U_Offset = DOffsetU.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].coords.V_Offset = DOffsetV.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].coords.U_Tiling = DTileU.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].coords.V_Tiling = DTileV.value
					meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].opacity=(Scale_Diff.value *100)
					meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[2]=5
					
				)

				if TexMap_DISPL.text !="" do
				(
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[2].map.sourceMap.filename =(TexMapPath.text+TexMap_Diff.text)
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[2].map.outputChannelIndex = 1
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[2].rewireR=4
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[2].rewireG=5
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[2].rewireB=6
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[2].rewireA=3
					
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].blendMode[2]=5
					
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[1].sourceMap.filename =(TexMapPath.text+TexMap_Diff.text)
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[1].outputChannelIndex = 5
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[1].sourceMap.Pos_map.heightTexMap = (TexMapPath.text+TexMap_DISPL.text)
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[1].sourceMap.Pos_map.depth_mult = (POMdisp.value*100)	
					meditMaterials[activeMeditSlot].texmap_opacity.maplist[1].maplist[1].sourceMap.Pos_map.minLayers = 16		
					
					meditMaterials[activeMeditSlot].option_doubleSided = off
					meditMaterials[activeMeditSlot].option_useIrradMap = off
					meditMaterials[activeMeditSlot].option_opacityMode = 0
					meditMaterials[activeMeditSlot].option_traceRefraction = on
					meditMaterials[activeMeditSlot].reflection_lockIOR = off
					meditMaterials[activeMeditSlot].reflection_IOR = 1.45
					meditMaterials[activeMeditSlot].Refraction=color 255 255 255 


				)
				if applygloss.state==false do
				(
					meditMaterials[activeMeditSlot].texmap_reflectionGlossiness= undefined

				)
				
			)
			if iskindof meditMaterials[activeMeditSlot] VRayMtl do  
			(
				meditMaterials[activeMeditSlot].texmap_bump_multiplier = 100
				meditMaterials[activeMeditSlot].option_opacityMode = 0
			)
	)
)
CreateDialog POMLoader
