maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"
cache_path = getINISetting maxINI "SCTools" "cache_path"
script_path = getINISetting maxINI "SCTools" "script_path"
json_path = getINISetting maxINI "SCTools" "json_path"

rollout SCGlow "Untitled" width:616 height:300
(


	bitmap 'bmp1' "Bitmap" pos:[107,8] width:380 height:60 fileName:"glow.tif" align:#left
	label 'lbl3' "Emission Color" pos:[472,128] width:88 height:16 align:#left
	groupBox 'lb_name' "Name" pos:[24,72] width:376 height:72 align:#left
	groupBox 'box_Glow' "Glow Properties" pos:[416,72] width:184 height:168 align:#left
	groupBox 'grp4' "Map Paths" pos:[24,152] width:376 height:136 align:#left
	button 'btn_make' "Import Material" pos:[428,249] width:170 height:38 align:#left
	editText 'Name_MTL' "MTL Name" pos:[56,88] width:208 height:20 align:#left
	editText 'Material_Name' "Material Name" pos:[40,112] width:280 height:20 align:#left
	editText 'TexMap_Diff' "Diffuse Map" pos:[40,176] width:328 height:20 align:#left
	editText 'TexPath' "Texture Path" pos:[40,240] width:328 height:20 align:#left text:tex_path--SET THIS TO YOUR TEXTURE PATH
	checkbox 'chk_alpha' "Apply Alpha to Opac" pos:[104,200] width:120 height:24 align:#left
	spinner 'GlowAmount' "Glow Amount" pos:[432,96] width:57 height:16 align:#left
	spinner 'Illum_R' "R" pos:[448,152] width:103 height:16 range:[0,1,0] scale:0.0001 align:#left
	spinner 'Illum_G' "G" pos:[448,176] width:103 height:16 range:[0,1,0] scale:0.0001 align:#left
	spinner 'Illum_B' "B" pos:[448,200] width:104 height:16 range:[0,1,0] scale:0.0001 align:#left
	
	on btn_make pressed do
	(
		meditMaterials[activeMeditSlot]=VRayLightMtl ()
		meditMaterials[activeMeditSlot].color=color (Illum_R.value*255) (Illum_G.value*255) (Illum_B.value*255) 
		meditMaterials[activeMeditSlot].texmap=CompositeTexturemap()
		meditMaterials[activeMeditSlot].texmap.maplist[1]=VRayBitmap()
		meditMaterials[activeMeditSlot].texmap.maplist[1].HDRIMapName=(TexPath.text+TexMap_Diff.text)
		meditMaterials[activeMeditSlot].texmap.maplist[2]=ColorCorrection()
		meditMaterials[activeMeditSlot].texmap.maplist[2].color=color (Illum_R.value*255) (Illum_G.value*255) (Illum_B.value*255) 255
		meditMaterials[activeMeditSlot].texmap.blendmode[2]=5
		meditMaterials[activeMeditSlot].twoSided=true 
		meditMaterials[activeMeditSlot].compensate_exposure=true
		meditMaterials[activeMeditSlot].multiplier=(GlowAmount.value*10)
		
		if chk_alpha.state==on do
		(
		)
		meditMaterials[activeMeditSlot].opacity_multiplyColor=true
		meditMaterials[activeMeditSlot].opacity_texmap=VRayBitmap()
		meditMaterials[activeMeditSlot].opacity_texmap.HDRIMapName=(TexPath.text+TexMap_Diff.text)
		meditMaterials[activeMeditSlot].opacity_texmap.color_space=0
		meditMaterials[activeMeditSlot].opacity_texmap.alphaSource=0
		meditMaterials[activeMeditSlot].opacity_texmap.rgbOutput = 1
		meditMaterials[activeMeditSlot].opacity_texmap.monoOutput = 1
		meditMaterials[activeMeditSlot].opacity_texmap_on=on
	)

)

CreateDialog SCGlow
