try (closeRolloutFloater SCDecal) catch()

maxINI = getMAXIniFile()
tex_path = getINISetting maxINI "SCTools" "tex_path"


rollout SCDecal "SC Decal Material Loader alpha 0.0.2" width:812 height:390
(
	bitmap 'bmp_name' "Bitmap" pos:[21,10] width:768 height:80 enabled:true fileName:"decalLoader.tiff" align:#left
	editText 'DecalName' "Decal Name" pos:[26,141] width:350 height:20 align:#left
	editText 'TexMap_Diff' "Diffuse [TextSlot1]" pos:[16,233] width:384 height:20 align:#left
	groupBox 'grp92' "Diffuse [1.0]" pos:[8,168] width:240 height:64 align:#left
	groupBox 'grp93' "Specular [1.0]" pos:[256,168] width:240 height:64 align:#left
	groupBox 'grp94' "Gloss [255]" pos:[504,168] width:128 height:64 align:#left
	editText 'TexMap_ddna' "Normal [TextSlot2]" pos:[16,258] width:384 height:20 align:#left
	groupBox 'grp95' "Glow [1.0]" pos:[640,168] width:128 height:64 align:#left
	groupBox 'grp96' "TexturePath" pos:[16,320] width:448 height:56 align:#left
	bitmap 'bmp_logo' "Bitmap" pos:[703,237] width:80 height:80 fileName:"Logo_Community_logo.tiff" align:#left
	button 'btn_make' "Import Texture" pos:[576,328] width:208 height:40 align:#left
	spinner 'Glow_Input' "" pos:[664,192] width:72 height:16 range:[0,100,0] align:#left
	spinner 'GlossMultiplier' "" pos:[528,192] width:80 height:16 range:[0,255,255] align:#left
	spinner 'Spec_R' "R" pos:[264,192] width:64 height:16 align:#left range:[0,1,1]
	spinner 'Spec_G' "G" pos:[336,192] width:64 height:16 range:[0,1,1] align:#left
	spinner 'Spec_B' "B" pos:[416,192] width:65 height:16 range:[0,1,1] align:#left
	spinner 'Diff_R' "R" pos:[24,192] width:64 height:16 range:[0,1,1] align:#left
	spinner 'Diff_G' "G" pos:[96,192] width:64 height:16 range:[0,1,1] align:#left
	spinner 'Diff_B' "B" pos:[176,192] width:65 height:16 range:[0,1,1] align:#left
	editText 'TexMapPath' "" pos:[32,336] width:416 height:24 align:#left text: tex_path --SET THIS TO YOUR TEXTURE PATH
	editText 'TexMap_Detail' "Detail Map" pos:[404,248] width:288 height:22 align:#left
	spinner 'DetailTile' "Detail Tile" pos:[475,280] width:64 height:16 align:#left range:[0,500,1]
	checkbox 'chk8_control' "Control Detail Map" pos:[557,307] width:139 height:18 align:#left
	groupBox 'grp118' "Detail Scale" pos:[461,108] width:292 height:47 align:#left
	spinner 'Scale_Diff' "Diff  " pos:[473,128] width:59 height:16 align:#left
	spinner 'Scale_Gloss' "Gloss  " pos:[563,128] width:49 height:16 range:[0,100,0] align:#left
	spinner 'Scale_DDNA' "Normal" pos:[656,128] width:48 height:16 range:[0,100,0] align:#left
	editText 'TexMap_Spec' "Specular [TextSlot2]" pos:[14,280] width:386 height:20 align:#left
	editText 'MTL_Name' "MTL Name" pos:[33,107] width:241 height:20 align:#left
	
	on btn_make pressed do
	(
		if TexMap_Diff.text!="" do
		(
			meditMaterials[activeMeditSlot]=VRayMtl()
			meditMaterials[activeMeditSlot].name=(MTL_Name.text+"_mtl_"+DecalName.text)
			meditMaterials[activeMeditSlot].texmap_diffuse=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3]=colorcorrection()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[3].color=color (Diff_R.value*255)(Diff_G.value*255)(Diff_B.value*255) 255
			meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[3]=5
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].HDRIMapName=(TexMapPath.text+TexMap_Diff.text)
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].alphaSource=2
			meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[4]=5
			meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[2]=5
			meditMaterials[activeMeditSlot].texmap_bump_multiplier = 100
			
			
			meditMaterials[activeMeditSlot].texmap_opacity=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_opacity.HDRIMapName=(TexMapPath.text+TexMap_Diff.text)
			meditMaterials[activeMeditSlot].texmap_opacity.color_space=0
			meditMaterials[activeMeditSlot].texmap_opacity.alphaSource=0
			meditMaterials[activeMeditSlot].texmap_opacity.rgbOutput = 1
			meditMaterials[activeMeditSlot].texmap_opacity.monoOutput = 1
			meditMaterials[activeMeditSlot].option_opacityMode = 0
			meditMaterials[activeMeditSlot].texmap_reflection=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_reflection.maplist[2]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_reflection.maplist[2].color= color (Spec_R.value*255)(Spec_G.value*255)(Spec_B.value*255) 255
			meditMaterials[activeMeditSlot].texmap_reflection.blendMode[2]=15
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.opacity[1]=((GlossMultiplier.value/255)*100)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[3]=5
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[2]=5
						
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[5]=colorcorrection()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[5].name="DIRTCOLOR"
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[5].color= color 73 53 26 255
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[5]=CompositeTexturemap()
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[5].maplist[1]=Noise()
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[5].opacity[1]=0
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[5].maplist[1].size=1.5
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[5].maplist[1].thresholdHigh=0.58
			meditMaterials[activeMeditSlot].texmap_diffuse.mask[5].maplist[1].thresholdLow=0.0
			

		)
		if Glow_Input.value >0 do
		(
			meditMaterials[activeMeditSlot].texmap_self_illumination=CompositeMap()	
			meditMaterials[activeMeditSlot].texmap_self_illumination.maplist[2].color=(Diff_R.value*255)(Diff_G.value*255)(Diff_B.value*255) 255
			meditMaterials[activeMeditSlot].texmap_self_illumination.blendMode[2]=5
			meditMaterials[activeMeditSlot].texmap_self_illumination.maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_self_illumination.maplist[1].HDRIMapName=(TexMapPath.text+TexMap_Diff.text)
			meditMaterials[activeMeditSlot].texmap_self_illumination.maplist[1].alphaSource=2
		)
		if TexMap_ddna.text !="" do
		(	BNormString=TexMap_ddna.text
			Bl_GlossPath=substring BNormString 1 (BNormString.count-4)+"_glossMap.tif"
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1]=VrayBitmap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].HDRIMapName=(TexMapPath.text+Bl_GlossPath)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].color_space=0
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].alphaSource=0
			meditMaterials[activeMeditSlot].texmap_bump=VRayNormalMap()
			meditMaterials[activeMeditSlot].texmap_bump.normal_map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_bump.normal_map.HDRIMapName=(TexMapPath.text+TexMap_ddna.text)
			meditMaterials[activeMeditSlot].texmap_bump.normal_map.color_space=0
		)
		if TexMap_Detail.text !="" and chk8_state==true do
		(
			meditMaterials[activeMeditSlot].texmap_bump.bump_map=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireR=1
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireG=3
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireB=9
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].rewireA=3
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.HDRIMapName=(TexMapPath.text+DetailMap.text)
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.alphaSource=0
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.U_Offset = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.maplist[1].map.coords.V_Offset = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_bump.maplist[2].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].texmap_bump.blendMode[2]=23
			meditMaterials[activeMeditSlot].texmap_bump.bump_map_multiplier=(Scale_DDNA.value*100)
			--diff layer
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireR=0
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireB=10
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.HDRIMapName=(TexMapPath.text+DetailMap.text)
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.color_space=0
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.coords.U_Offset = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[1].map.coords.V_Offset = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_diffuse.blendMode[2]=5
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].opacity=(Scale_Diff.value *100)
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].blendMode[2]=23
			--gloss layer
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2]=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireR=10
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireG=10
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireB=0
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].rewireA=9
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.HDRIMapName=(TexMapPath.text+DetailMap.text)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.color_space=0.0
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.U_Tiling =DetailTile.value
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[1].map.coords.V_Tiling = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].blendMode[2]=14
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].opacity=(Scale_Gloss.value *100)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].maplist[2]=RGB_Tint()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2]blendMode[2]=23

		)
		if TexMap_Detail.text !="" and chk8_state==false do
		(
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2]=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].HDRIMapName=(TexMapPath.text+DetailMap.text)
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].color_space=0.0
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].opacity=(DetailScale_Diff.value *100)
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].coords.U_Tiling = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[2].coords.V_Tiling = DetailTile.value
			--gloss
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2]=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].HDRIMapName=(TexMapPath.text+DetailMap.text)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].color_space=0.0
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].opacity=(Scale_Gloss.value *100)
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.U_Tiling = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[2].coords.V_Tiling = DetailTile.value
			--ddna
			meditMaterials[activeMeditSlot].texmap_bump.bump_map=CompositeTextureMap()
			meditMaterials[activeMeditSlot].texmap_bump.bump_map=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.HDRIMapName=(TexMapPath.text+DetailMap.text)
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.color_space=0.0
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.opacity=(Scale_Gloss.value *100)
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.U_Tiling = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.V_Tiling = DetailTile.value
			meditMaterials[activeMeditSlot].texmap_bump.bump_map_multiplier=(Scale_DDNA.value*100)
		)
		if TexMap_Spec.text!="" do
		(
			meditMaterials[activeMeditSlot].texmap_reflection.maplist[1]=VRayBitmap()
			meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].HDRIMapName=(TexMapPath.text+TexMap_Spec.text)
			meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].color_space=0.0
		)
		
		if TexMap_ddna.text=="" do
		(
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1]=ColorCorrection()
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].color=color 255 255 255 255
		)
			
			

	)
	
	
)
CreateDialog SCDecal
