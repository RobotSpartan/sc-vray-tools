rollout MATAid "MAT Load Aid" width:340 height:408
(
	button 'btn_VrayMtl' "Set to VRay(WIP)" pos:[69,37] width:175 height:47 align:#left
	button 'btn17' "Set to VRayBlend(WIP)" pos:[69,102] width:175 height:47 align:#left
	button 'btn18' "Set to VRayLight(WIP)" pos:[69,167] width:175 height:47 align:#left
	groupBox 'How it Works' "Instructions" pos:[13,240] width:311 height:117 align:#left
	label 'lbl4' "Library Load Tool" pos:[21,5] width:299 height:26 align:#left
	label 'lbl5' "This converts Standard, VRay, VRayBlend, VRay Light materils with a diffuse of (255,0,255) or (1,0,1) to the selected material. This aids in loading files from a .mat." pos:[23,257] width:288 height:91 align:#left
	button 'btn_StandardtoMag' "Set all Standard Materials as Magenta" pos:[71,364] width:176 height:37 align:#left
	

	on btn_StandardtoMag pressed do
	(
			for m in getClassInstances standardmaterial do
		(
		m.Diffuse=color 255 0 255  	
		)
	)
	
	on btn_VrayMtl pressed do
	(

	)
	
	
	
	
)
CreateDialog MATAid 