try (closeRolloutFloater SC_layertileadjust) catch()

rollout SC_layertileadjust "Layer Tile Adjuster/Fixer" width:300 height:150
(
	spinner 'tileamount' "New Tile Value"pos:[130,30] width:60 height:20 range:[0,100,8] scale:0.01
	button 'Adjust' "Adjust" pos: [100, 60] width: 120 height: 40
	
	on Adjust pressed do
	(
		meditMaterials[activeMeditSlot].texmap_bump_multiplier = 100
		
		if iskindof meditMaterials[activeMeditSlot].texmap_diffuse undefined do
		(
			iskindof meditMaterials[activeMeditSlot].texmap_diffuse=CompositeTexturemap ()
			iskindof meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1]=CompositeTextureMap()
		)
		
		if iskindof meditMaterials[activeMeditSlot].texmap_reflectionGlossiness undefined do
		(
			iskindof meditMaterials[activeMeditSlot].texmap_reflectionGlossiness=CompositeTexturemap ()
			iskindof meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1]=CompositeTextureMap()
		)
		
		if iskindof meditMaterials[activeMeditSlot].texmap_bump undefined do
		(
			iskindof meditMaterials[activeMeditSlot].texmap_bump=VRayNormalMap()
		)
			
		
		if iskindof meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1] VRayBitmap do
		(
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1].coords.U_Tiling = tileamount.value
			meditMaterials[activeMeditSlot].texmap_diffuse.maplist[1].maplist[1].coords.V_Tiling = tileamount.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].texmap_reflection.maplist[1] VRayBitmap do
		(
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].coords.U_Tiling = tileamount.value
				meditMaterials[activeMeditSlot].texmap_reflection.maplist[1].coords.V_Tiling = tileamount.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1] VRayBitmap do
		(
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.U_Tiling = tileamount.value
			meditMaterials[activeMeditSlot].texmap_reflectionGlossiness.maplist[1].maplist[1].coords.V_Tiling = tileamount.value
		)
		
		if iskindof meditMaterials[activeMeditSlot].texmap_bump.bump_map VRayBitmap do
		(
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.U_Tiling = tileamount.value
			meditMaterials[activeMeditSlot].texmap_bump.bump_map.coords.V_Tiling = tileamount.value
		)
		
		
	)
)
CreateDialog SC_layertileadjust
