import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 

maxINI = rt.getMAXIniFile()
tex_path = rt.getINISetting(maxINI, "SCTools", "tex_path")
cache_path = rt.getINISetting(maxINI, "SCTools", "cache_path")
script_path = rt.getINISetting(maxINI, "SCTools", "script_path")
json_path = rt.getINISetting(maxINI, "SCTools", "json_path")

filematWE=open(cache_path + "\WS2m.txt", 'r') #Set to your temp folder
matId=int(filematWE.read())
filematWE.close()
    
print(matId)


filemtl=open(cache_path + "\WSGetMtl.txt", 'r') #Set to your temp folder
Base_MTL_Load=str(filemtl.read())
filemtl.close()
Base_MTL_Load = Base_MTL_Load.replace("\n",'')
Base_MTL_Load = Base_MTL_Load.replace('\\', '/')
Base_MTL_Load = Base_MTL_Load.replace('"', '')

print(Base_MTL_Load)



SC_Base = json.load(open(Base_MTL_Load))


#Name
Base_Name=(SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Name'))


#Pull Parameters
    
try:
    Dirt_Color= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DirtColor')) 
except KeyError:
    Dirt_Color=None
    
if not Dirt_Color == None:    
    DirtRGB_STR=Dirt_Color.split(",")
    DirtR=DirtRGB_STR[0]
    DirtG=DirtRGB_STR[1]
    DirtB=DirtRGB_STR[2]
else:
    DirtR=None
    DirtG=None
    DirtB=None
    

try:
    BL1_TintRGB= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DiffuseTint1')) 
except KeyError:
    BL1_TintRGB=None
    
if not BL1_TintRGB == None:    
    BL1_TintRGB_STR=BL1_TintRGB.split(",")
    BL1_TintR=BL1_TintRGB_STR[0]
    BL1_TintG=BL1_TintRGB_STR[1]
    BL1_TintB=BL1_TintRGB_STR[2]
else:
    BL1_TintR=None
    BL1_TintG=None
    BL1_TintB=None

try:
    WL1_TintRGB= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DiffuseTint1')) 
except KeyError:
    WL1_TintRGB=None
    
if not WL1_TintRGB == None:    
    WL1_TintRGB_STR=WL1_TintRGB.split(",")
    WL1_TintR=WL1_TintRGB_STR[0]
    WL1_TintG=WL1_TintRGB_STR[1]
    WL1_TintB=WL1_TintRGB_STR[2]
else:
    WL1_TintR=None
    WL1_TintG=None
    WL1_TintB=None


try:
    BL1_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@GlossMult1')) 
except KeyError:
    BL1_Gloss=None

try:
    WL1_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@GlossMultWear1')) 
except KeyError:
    WL1_Gloss=None

try:
    BL1_UVTiling= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@TilingScale1')) 
except KeyError:
    BL1_UVTiling=None

try:
    WL1_UVTiling= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@TilingScaleWear1')) 
except KeyError:
    WL1_UVTiling=None



    #BlendMask



#baseblendLayer
def search_texmap (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']

# MatLayer


def tint_path (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatReferences']['MatRef']):
  if name.lower() == keyval['@Slot'].lower():
   return keyval['@File']

#Textmaps
try:
    Base_TexMap_DDNA=search_texmap ('Custom')
except KeyError:
    Base_TexMap_DDNA=None
    

try:
    Base_TexMap_WDA=search_texmap ('[1] Custom')
except KeyError:
    Base_TexMap_WDA=None
    
#LayerPaths
try:
    BL1_Path= tint_path ("0")
except KeyError:
    BL1_Path=None

try:
    WL1_Path= tint_path ("4")
except KeyError:
    WL1_Path=None








EExport=[
'mtlname="',(Base_Name or ""),'"\n',
#maps
'Base_TexMap_DDNA="',(Base_TexMap_DDNA or ""),'"\n',
'Base_TexMap_WDA="',(Base_TexMap_WDA or ""),'"\n',
#numbers

#Master Scale

#Dirt
'DirtR="',(DirtR or ""),'"\n',
'DirtG="',(DirtG or ""),'"\n',
'DirtB="',(DirtB or ""),'"\n',

'BL1_TintR="',(BL1_TintR or ""),'"\n',
'BL1_TintG="',(BL1_TintG or ""),'"\n',
'BL1_TintB="',(BL1_TintB or ""),'"\n',

'WL1_TintR="',(WL1_TintR or ""),'"\n',
'WL1_TintG="',(WL1_TintG or ""),'"\n',
'WL1_TintB="',(WL1_TintB or ""),'"\n',

#Gloss
'BL1_Gloss="',(BL1_Gloss or ""),'"\n',
'WL1_Gloss="',(WL1_Gloss or ""),'"\n',

#tile
'BL1_UVTiling="',(BL1_UVTiling or ""),'"\n',
'WL1_UVTiling="',(WL1_UVTiling or ""),'"\n',


#paths
'BL1_Path="',(BL1_Path or "blank"),'"\n',
'WL1_Path="',(WL1_Path or "blank"),'"\n',


]

fileWE=open(cache_path + 'WSOutput.txt', 'w') #Set to your temp folder
fileWE.writelines(EExport)
fileWE.close()



filematWE.close()

gc.collect()
