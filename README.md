# SC_3DSMAX_Tool_VRAY
Material and Lighting toolset to aid in recreating Star Citizen materials in 3DS Max with VRay Materials

README Instructions: https://drive.google.com/file/d/17KRyxFC2s-wBGDsQ2pbP3F6ua8e5bHB9/view?usp=sharing

POM uses Martin Geupel (www.racoon-artworks.de) OSL

New “Auto-Field” loader:

This new feature will auto-load almost all the fields, but not all of them (some tiling information is not loaded). 

The feature relies on .txt file to give instructions from the Maxscript to several python scripts. So you may encounter the fulling errors if things are not done correctly.

1.)Crashes when trying to load data that is not available. Requires a complete shutdown and restart of 3ds Max.

2.)Accessing previous instructions from previous materials if the current material doesn't require instructions for that parameter.

3.)Trying to load decal or glow/light materials will cause a light crash. Close the script and reopen it.

Installation Instructions:
-As with all my scripts you will need to adjust your paths to match up with your system setup. This includes placing a new temporary folder to hold the instructions to and from scripts.


-The scripts read the Cry Engine .mtl in JSON format, so you will need to make sure you export your mtl files in JSON. I personally export my .mtl files as XML and place them in my master file reservoir, and create separate folder containing my .mtl in JSON format. You will see this reflected in my scripts.

Use Instructions:

-Select MTL ID and then LOAD and select the mtl you wish to load from (this creates instructions the python script to pull that individual material data). This is a bug that needs to be worked on, eventual you'll only need to load the mtl once 



Video Series: https://www.youtube.com/playlist?list=PLr5XU4B6R4l6W5QOhHn3b7-qtATMgeiUo
