import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 

maxINI = rt.getMAXIniFile()
tex_path = rt.getINISetting(maxINI, "SCTools", "tex_path")
cache_path = rt.getINISetting(maxINI, "SCTools", "cache_path")
script_path = rt.getINISetting(maxINI, "SCTools", "script_path")
json_path = rt.getINISetting(maxINI, "SCTools", "json_path")

#Change the beginning of your directory to your JSON MTL location
with open (cache_path + "\Layer2Input.txt", 'r') as file:
    Layer2mtl=file.read()
    file.close()



L2_MTL_Load=open (json_path + Layer2mtl)#Insert your JSON directory

SC_L2 = json.load(L2_MTL_Load)

#

try:
    Layer2_Pat=(SC_L2 ['Material']['Textures']['Texture'])
except KeyError:
    Layer2_Pat=None
    

try:
    L2_SpecularColor= (SC_L ['Material']['@Specular'])
except KeyError:
    L2_SpecularColor=None

if not L2_SpecularColor ==None:
    L2_SpecRGB_STR=L2_SpecularColor.split(",")
    L2_SpecR=L2_SpecRGB_STR[0]
    L2_SpecG=L2_SpecRGB_STR[1]
    L2_SpecB=L2_SpecRGB_STR[2]
else:
    L2_SpecR=None
    L2_SpecG=None
    L2_SpecB=None
    
#Pull Diffuse
try:
    L2_DiffColor= (SC_L ['Material']['@Diffuse'])
except KeyError:
    L2_DiffColor=None
if not L2_DiffColor==None:
    L2_DiffRGB_STR=L2_DiffColor.split(",")
    L2_DiffR=L2_DiffRGB_STR[0]
    L2_DiffG=L2_DiffRGB_STR[1]
    L2_DiffB=L2_DiffRGB_STR[2]
else:
    L2_DiffR=None
    L2_DiffG=None
    L2_DiffB=None
    
#Pull Gloss
try:
    L2_Gloss= (SC_L ['Material']['@Shininess'])
except KeyError:
    L2_Gloss=None
#Pull Detail Tile
try:
    L2_DTile=(SC_L ['Material']['PublicParams']['@DetailTiling'])
except KeyError:
    L2_DTile=None
#Detail Diffuse
try:
    L2_DetaiL2_Diff=(SC_L ['Material']['PublicParams']['@DetailDiffuse'])
except KeyError:
    L2_DetaiL2_Diff=None
#Detail Normal
try:
    L2_DetaiL2_Nrm=(SC_L ['Material']['PublicParams']['@DetailBump'])
except KeyError:
    L2_DetaiL2_Nrm=None
#Detail Gloss
try:
    L2_DetaiL2_Gloss=(SC_L ['Material']['PublicParams']['@DetailGloss'])
except KeyError:
    L2_DetaiL2_Gloss=None
    
try:
    L2_Detail_Tile=(SC_L ['Material']['PublicParams']['@DetailTiling'])
except KeyError:
    L2_Detail_Tile=None

try:
    L2_ClearCoat=(SC_L ['Material']['PublicParams']['@ClearCoatBlend'])
except KeyError:
    L2_ClearCoat=None


def search_texmap (name):
 for keyval in Layer2_Pat:
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']


try:
    L2_TextMapDiff=search_texmap ('TexSlot1')
except KeyError:
    L2_TextMapDiff=None
try:
    L2_TextMapNormal=search_texmap ('TexSlot2')
except KeyError:
    L2_TextMapNormal=None
try:
    L2_TextMapDetail=search_texmap ('TexSlot7')
except KeyError:
    L2_TextMapDetail=None
try:
    L2_TextMapSpec=search_texmap ('TexSlot6')
except KeyError:
    L2_TextMapSpec=None

print('Layer_2_Diff="',(L2_TextMapDiff or ""),'"\n',
'Layer_2_Normal="',(L2_TextMapNormal or ""),'"\n',
'Layer_2_Detail="',(L2_TextMapDetail or ""),'"\n',
'Layer_2_SpecR=',(L2_SpecR or ""),'\n',
'Layer_2_SpecG=',(L2_SpecG or ""),'\n',
'Layer_2_SpecB=',(L2_SpecB or ""),'\n',
'Layer_2_DiffR=',(L2_DiffR or ""),'\n',
'Layer_2_DiffG=',(L2_DiffG or ""),'\n',
'Layer_2_DiffB=',(L2_DiffB or ""),'\n',
'Layer_2_Gloss=',(L2_Gloss or ""),'\n',
'Layer_2_Detail_diff_Scale=',(L2_DetaiL2_Diff or ""),'\n',
'Layer_2_Detail_ddna_Scale=',(L2_DetaiL2_Nrm or ""),'\n',
'Layer_2_Detail_gloss_Scale=',(L2_DetaiL2_Gloss or ""),'\n')

ZExport=['Layer_2_Diff="',(L2_TextMapDiff or ""),'"\n',
'Layer_2_Normal="',(L2_TextMapNormal or ""),'"\n',
'Layer_2_Detail="',(L2_TextMapDetail or ""),'"\n',
'Layer_2_SpecR=',(L2_SpecR or ""),'\n',
'Layer_2_SpecG=',(L2_SpecG or ""),'\n',
'Layer_2_SpecB=',(L2_SpecB or ""),'\n',
'Layer_2_DiffR=',(L2_DiffR or ""),'\n',
'Layer_2_DiffG=',(L2_DiffG or ""),'\n',
'Layer_2_DiffB=',(L2_DiffB or ""),'\n',
'Layer_2_Gloss=',(L2_Gloss or ""),'\n',
'Layer_2_Detail_diff_Scale=',(L2_DetaiL2_Diff or ""),'\n',
'Layer_2_Detail_ddna_Scale=',(L2_DetaiL2_Nrm or ""),'\n',
'Layer_2_Detail_gloss_Scale=',(L2_DetaiL2_Gloss or ""),'\n',
'Layer_2_Spec="',(L2_TextMapSpec or ""),'"\n',
'Layer_2_Dtile=',(L2_Detail_Tile or ""),'\n',
'Layer_2_CC=',(L2_ClearCoat or ""),'\n',
]


Layer2Output= open(cache_path + 'Layer2Output.txt', 'w')#Change to your local temp 
Layer2Output.writelines(ZExport)
Layer2Output.close()

L2_MTL_Load.close()

gc.collect()
