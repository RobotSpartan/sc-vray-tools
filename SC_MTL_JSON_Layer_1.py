import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 

maxINI = rt.getMAXIniFile()
tex_path = rt.getINISetting(maxINI, "SCTools", "tex_path")
cache_path = rt.getINISetting(maxINI, "SCTools", "cache_path")
script_path = rt.getINISetting(maxINI, "SCTools", "script_path")
json_path = rt.getINISetting(maxINI, "SCTools", "json_path")

#Change the beginning of your directory to your JSON MTL location
with open (cache_path + "\Layer1Input.txt", 'r') as file:
    Layer1mtl=file.read()
    file.close()





L_MTL_Load=open (json_path + Layer1mtl)#Insert your JSON directory

SC_L = json.load(L_MTL_Load)

#
try:
    Layer1_Pat=(SC_L ['Material']['Textures']['Texture'])
except KeyError:
    Layer1_Pat=None
    

#Pull Spec information
try:
    L_SpecularColor= (SC_L ['Material']['@Specular'])
except KeyError:
    L_SpecularColor=None

if not L_SpecularColor ==None:
    L_SpecRGB_STR=L_SpecularColor.split(",")
    L_SpecR=L_SpecRGB_STR[0]
    L_SpecG=L_SpecRGB_STR[1]
    L_SpecB=L_SpecRGB_STR[2]
else:
    L_SpecR=None
    L_SpecG=None
    L_SpecB=None
    
#Pull Diffuse
try:
    L_DiffColor= (SC_L ['Material']['@Diffuse'])
except KeyError:
    L_DiffColor=None
if not L_DiffColor==None:
    L_DiffRGB_STR=L_DiffColor.split(",")
    L_DiffR=L_DiffRGB_STR[0]
    L_DiffG=L_DiffRGB_STR[1]
    L_DiffB=L_DiffRGB_STR[2]
else:
    L_DiffR=None
    L_DiffG=None
    L_DiffB=None
    
#Pull Gloss
try:
    L_Gloss= (SC_L ['Material']['@Shininess'])
except KeyError:
    L_Gloss=None
#Pull Detail Tile
try:
    L_DTile=(SC_L ['Material']['PublicParams']['@DetailTiling'])
except KeyError:
    L_DTile=None
#Detail Diffuse
try:
    L_Detail_Diff=(SC_L ['Material']['PublicParams']['@DetailDiffuse'])
except KeyError:
    L_Detail_Diff=None
#Detail Normal
try:
    L_Detail_Nrm=(SC_L ['Material']['PublicParams']['@DetailBump'])
except KeyError:
    L_Detail_Nrm=None
#Detail Gloss
try:
    L_Detail_Gloss=(SC_L ['Material']['PublicParams']['@DetailGloss'])
except KeyError:
    L_Detail_Gloss=None
    
try:
    L_Detail_Tile=(SC_L ['Material']['PublicParams']['@DetailTiling'])
except KeyError:
    L_Detail_Tile=None

try:
    L_ClearCoat=(SC_L ['Material']['PublicParams']['@ClearCoatBlend'])
except KeyError:
    L_ClearCoat=None
    
    
def search_texmap (name):
 for keyval in Layer1_Pat:
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']

try:
    L_TextMapDiff=search_texmap ('TexSlot1')
except KeyError:
    L_TextMapDiff=None
try:
    L_TextMapNormal=search_texmap ('TexSlot2')
except KeyError:
    L_TextMapNormal=None
try:
    L_TextMapDetail=search_texmap ('TexSlot7')
except KeyError:
    L_TextMapDetail=None
try:
    L_TextMapSpec=search_texmap ('TexSlot6')
except KeyError:
    L_TextMapSpec=None

print('Layer_1_Diff="',(L_TextMapDiff or ""),'"\n',
'Layer_1_Normal="',(L_TextMapNormal or ""),'"\n',
'Layer_1_Detail="',(L_TextMapDetail or ""),'"\n',
'Layer_1_SpecR=',(L_SpecR or ""),'\n',
'Layer_1_SpecG=',(L_SpecG or ""),'\n',
'Layer_1_SpecB=',(L_SpecB or ""),'\n',
'Layer_1_DiffR=',(L_DiffR or ""),'\n',
'Layer_1_DiffG=',(L_DiffG or ""),'\n',
'Layer_1_DiffB=',(L_DiffB or ""),'\n',
'Layer_1_Gloss=',(L_Gloss or ""),'\n',
'Layer_1_Detail_diff_Scale=',(L_Detail_Diff or ""),'\n',
'Layer_1_Detail_ddna_Scale=',(L_Detail_Nrm or ""),'\n',
'Layer_1_Detail_gloss_Scale=',(L_Detail_Gloss or ""),'\n')

ZExport=['Layer_1_Diff="',(L_TextMapDiff or ""),'"\n',
'Layer_1_Normal="',(L_TextMapNormal or ""),'"\n',
'Layer_1_Detail="',(L_TextMapDetail or ""),'"\n',
'Layer_1_SpecR=',(L_SpecR or ""),'\n',
'Layer_1_SpecG=',(L_SpecG or ""),'\n',
'Layer_1_SpecB=',(L_SpecB or ""),'\n',
'Layer_1_DiffR=',(L_DiffR or ""),'\n',
'Layer_1_DiffG=',(L_DiffG or ""),'\n',
'Layer_1_DiffB=',(L_DiffB or ""),'\n',
'Layer_1_Gloss=',(L_Gloss or ""),'\n',
'Layer_1_Detail_diff_Scale=',(L_Detail_Diff or ""),'\n',
'Layer_1_Detail_ddna_Scale=',(L_Detail_Nrm or ""),'\n',
'Layer_1_Detail_gloss_Scale=',(L_Detail_Gloss or ""),'\n',
'Layer_1_Spec="',(L_TextMapSpec or ""),'"\n',
'Layer_1_Dtile=',(L_Detail_Tile or ""),'\n',
'Layer_1_CC=',(L_ClearCoat or ""),'\n'
]


Layer1Output = open(cache_path + 'Layer1Output.txt', 'w')#Change to your local temp 
Layer1Output.writelines(ZExport)
Layer1Output.close()

L_MTL_Load.close()

gc.collect()
