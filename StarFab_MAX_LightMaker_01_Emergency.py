''''
Py tamplet by ventorvar
Completed by Baconator650

To Use:Load lights into a sperate .max scene and save, then load a your ship/OC model and replace(merge) the lights imported by this script into your ship/OC scene
Design to work with Standard Lights for better compatiblity with multiple render [may have to enable legacy modes on certain renders]
'''
import json
import pymxs #MaxScript Wrapper (disable if not using within 3ds Max) 
from pymxs import runtime as rt #MaxScript Wrapper (disable if not using within 3ds Max) 

SCBPLoad=rt.getOpenFileName(caption="Get that SCBP:",types="Star Citizen BluePrints (*.scbp)|*.scbp|""All those other Files|*.*")

PowerModifier=100 #Set to 10,0 for Vray Render; 1.0 for scanline, mental ray, or Cry Engine (1;1)
ScaleModifier=100 #Adjust for unit scale. 
KLightLoad=open(SCBPLoad)
bp = json.load(KLightLoad)

for soc_name, soc in bp['socs'].items():
    for lg_name, light_group in soc['lights'].items():
        for light_name, light in light_group.items():
            light_radius = float(light['EntityComponentLight']['sizeParams'].get('@lightRadius', 1.0))*ScaleModifier
            intensity = float(light['EntityComponentLight']['emergencyState'].get('@intensity', 1.0))*PowerModifier
            bulb_radius = float(light['EntityComponentLight']['sizeParams'].get('@bulbRadius', .01))*ScaleModifier
            #color
            Color_R = float(light['EntityComponentLight']['emergencyState']['color']['@r'])
            Color_G = float(light['EntityComponentLight']['emergencyState']['color']['@g'])
            Color_B = float(light['EntityComponentLight']['emergencyState']['color']['@b'])
            #projector
            FOV= float(light['EntityComponentLight']['projectorParams'].get('@FOV', 179.5))
            Focal_Beam= float(light['EntityComponentLight']['projectorParams'].get('@focusedBeam', 1.0))
            #light texture
            Light_Tex_Path=light['EntityComponentLight']['projectorParams']['@texture']
            texfiletypez='.tif'
            TextDDS_TIF=Light_Tex_Path.replace('.dds',texfiletypez)


            if light['EntityComponentLight']['@lightType'] == 'Projector':
                pymxs.runtime.freeSpot(
                rgb=rt.color(Color_R*255, Color_G*255, Color_B*255), 
                multiplier=intensity, 
                nearAttenStart=0.0, 
                nearAttenEnd=bulb_radius,
                farAttenStart=bulb_radius,
                farAttenEnd=light_radius,
                useNearAtten=rt.true,
                useFarAtten=rt.true,
                decayRadius=0.0, 
                hotspot=FOV, 
                falloff=FOV, 
                coneShape=2,
                projector=rt.true, 
                castShadows=rt.true,
                projectorMap=rt.bitmaptexture(filename=((r"D:/Star Citizen Master Texture/")+(TextDDS_TIF)), gamma=(1.0)),
                name=light_name)

            else:
                pymxs.runtime.Omnilight(
                rgb=rt.color(Color_R*255, Color_G*255, Color_B*255), 
                multiplier=intensity, 
                nearAttenStart=0.0, 
                nearAttenEnd=bulb_radius,
                farAttenStart=bulb_radius,
                farAttenEnd=light_radius,
                useNearAtten=rt.true,
                useFarAtten=rt.true,                
                decayRadius=0.0,
                castShadows=rt.true,
                name=light_name)

KLightLoad.close()
