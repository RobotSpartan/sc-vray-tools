import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 

maxINI = rt.getMAXIniFile()
tex_path = rt.getINISetting(maxINI, "SCTools", "tex_path")
cache_path = rt.getINISetting(maxINI, "SCTools", "cache_path")
script_path = rt.getINISetting(maxINI, "SCTools", "script_path")
json_path = rt.getINISetting(maxINI, "SCTools", "json_path")

filematWC=open(cache_path + "\WC2m.txt", 'r') #Set to your temp folder
matId=int(filematWC.read())
filematWC.close()
    
print(matId)


filemtl=open(cache_path + "\WCGetMtl.txt", 'r') #Set to your temp folder
Base_MTL_Load=str(filemtl.read())
filemtl.close()
Base_MTL_Load = Base_MTL_Load.replace("\n",'')
Base_MTL_Load = Base_MTL_Load.replace('\\', '/')
Base_MTL_Load = Base_MTL_Load.replace('"', '')

print(Base_MTL_Load)



SC_Base = json.load(open(Base_MTL_Load))


#Name
Base_Name=(SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Name'))


#Pull Parameters
try:
    Decal_SpecColor= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DiffuseDecalSpec')) 
except KeyError:
    Decal_SpecColor=None
    
if not Decal_SpecColor == None:    
    Decal_SpecRGB_STR=Decal_SpecColor.split(",")
    Decal_SpecR=Decal_SpecRGB_STR[0]
    Decal_SpecG=Decal_SpecRGB_STR[1]
    Decal_SpecB=Decal_SpecRGB_STR[2]
else:
    Decal_SpecR=None
    Decal_SpecG=None
    Decal_SpecB=None
    
    
    
try:
    Dirt_Color= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DirtColor')) 
except KeyError:
    Dirt_Color=None
    
if not Dirt_Color == None:    
    DirtRGB_STR=Dirt_Color.split(",")
    DirtR=DirtRGB_STR[0]
    DirtG=DirtRGB_STR[1]
    DirtB=DirtRGB_STR[2]
else:
    DirtR=None
    DirtG=None
    DirtB=None
    
try:
    Decal_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DiffuseDecalGloss')) 
except KeyError:
    Decal_Gloss=None


    #BlendMask



#baseblendLayer
def search_texmap (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']

# MatLayer


def tint_path (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@Path']

def tint_color (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@TintColor']
   
def tint_GlossMult (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@GlossMult']

def tint_UVtiling (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@UVTiling']


#Textmaps
try:
    Base_TexMap_DDNA=search_texmap ('TexSlot3')
except KeyError:
    Base_TexMap_DDNA=None
    
try:
    Base_TexMap_Decal=search_texmap ('TexSlot9')
except KeyError:
    Base_TexMap_Decal=None
    

try:
    Base_TexMap_WDA=search_texmap ('TexSlot11')
except KeyError:
    Base_TexMap_WDA=None
    

try:
    Base_TexMap_BLEND=search_texmap ('TexSlot12')
except KeyError:
    Base_TexMap_BLEND=None
    
#LayerPaths
try:
    BL1_Path= tint_path ("BaseLayer1")
except KeyError:
    BL1_Path=None

try:
    BL2_Path= tint_path ("BaseLayer2")
except KeyError:
    BL2_Path=None


try:
    BL3_Path= tint_path ("BaseLayer3")
except KeyError:
    BL3_Path=None


try:
    BL4_Path= tint_path ("BaseLayer4")
except KeyError:
    BL4_Path=None


try:
    WL1_Path= tint_path ("WearLayer1")
except KeyError:
    WL1_Path=None

try:
    WL2_Path= tint_path ("WearLayer2")
except KeyError:
    WL2_Path=None


try:
    WL3_Path= tint_path ("WearLayer3")
except KeyError:
    WL3_Path=None


try:
    WL4_Path= tint_path ("WearLayer4")
except KeyError:
    WL4_Path=None

#LayerGloss
try:
    BL1_Gloss= tint_GlossMult ("BaseLayer1")
except KeyError:
    BL1_Gloss=None

try:
    BL2_Gloss= tint_GlossMult ("BaseLayer2")
except KeyError:
    BL2_Gloss=None


try:
    BL3_Gloss= tint_GlossMult ("BaseLayer3")
except KeyError:
    BL3_Gloss=None


try:
    BL4_Gloss= tint_GlossMult ("BaseLayer4")
except KeyError:
    BL4_Gloss=None


try:
    WL1_Gloss= tint_GlossMult ("WearLayer1")
except KeyError:
    WL1_Gloss=None

try:
    WL2_Gloss= tint_GlossMult ("WearLayer2")
except KeyError:
    WL2_Gloss=None


try:
    WL3_Gloss= tint_GlossMult ("WearLayer3")
except KeyError:
    WL3_Gloss=None


try:
    WL4_Gloss= tint_GlossMult ("WearLayer4")
except KeyError:
    WL4_Gloss=None



#LayerUV
try:
    BL1_UVTiling= tint_UVtiling ("BaseLayer1")
except KeyError:
    BL1_UVTiling=None

try:
    BL2_UVTiling= tint_UVtiling ("BaseLayer2")
except KeyError:
    BL2_UVTiling=None


try:
    BL3_UVTiling= tint_UVtiling ("BaseLayer3")
except KeyError:
    BL3_UVTiling=None


try:
    BL4_UVTiling= tint_UVtiling ("BaseLayer4")
except KeyError:
    BL4_UVTiling=None


try:
    WL1_UVTiling= tint_UVtiling ("WearLayer1")
except KeyError:
    WL1_UVTiling=None

try:
    WL2_UVTiling= tint_UVtiling ("WearLayer2")
except KeyError:
    WL2_UVTiling=None


try:
    WL3_UVTiling= tint_UVtiling ("WearLayer3")
except KeyError:
    WL3_UVTiling=None


try:
    WL4_UVTiling= tint_UVtiling ("WearLayer4")
except KeyError:
    WL4_UVTiling=None



#LayerTintColor

try:
    BL1_TintColor=tint_color ('BaseLayer1')
except KeyError:
    BL1_TintColor=None
    
if not BL1_TintColor ==None:
    BL1_TintColor_str=BL1_TintColor.split(",")
    BL1_TintR=BL1_TintColor_str[0]
    BL1_TintG=BL1_TintColor_str[1]
    BL1_TintB=BL1_TintColor_str[2]
else:
    BL1_TintR=None
    BL1_TintG=None
    BL1_TintB=None
    
    
try:
    BL2_TintColor=tint_color ('BaseLayer2')
except KeyError:
    BL2_TintColor=None
    
if not BL2_TintColor ==None:
    BL2_TintColor_str=BL2_TintColor.split(",")
    BL2_TintR=BL2_TintColor_str[0]
    BL2_TintG=BL2_TintColor_str[1]
    BL2_TintB=BL2_TintColor_str[2]
else:
    BL2_TintR=None
    BL2_TintG=None
    BL2_TintB=None
try:
    BL3_TintColor=tint_color ('BaseLayer3')
except KeyError:
    BL3_TintColor=None
    
if not BL3_TintColor ==None:
    BL3_TintColor_str=BL3_TintColor.split(",")
    BL3_TintR=BL3_TintColor_str[0]
    BL3_TintG=BL3_TintColor_str[1]
    BL3_TintB=BL3_TintColor_str[2]
else:
    BL3_TintR=None
    BL3_TintG=None
    BL3_TintB=None
try:
    BL4_TintColor=tint_color ('BaseLayer4')
except KeyError:
    BL4_TintColor=None
    
if not BL4_TintColor ==None:
    BL4_TintColor_str=BL4_TintColor.split(",")
    BL4_TintR=BL4_TintColor_str[0]
    BL4_TintG=BL4_TintColor_str[1]
    BL4_TintB=BL4_TintColor_str[2]
else:
    BL4_TintR=None
    BL4_TintG=None
    BL4_TintB=None
    

try:
    WL1_TintColor=tint_color ('WearLayer1')
except KeyError:
    WL1_TintColor=None
    
if not WL1_TintColor ==None:
    WL1_TintColor_str=WL1_TintColor.split(",")
    WL1_TintR=WL1_TintColor_str[0]
    WL1_TintG=WL1_TintColor_str[1]
    WL1_TintB=WL1_TintColor_str[2]
else:
    WL1_TintR=None
    WL1_TintG=None
    WL1_TintB=None
    
    
try:
    WL2_TintColor=tint_color ('WearLayer2')
except KeyError:
    WL2_TintColor=None
    
if not WL2_TintColor ==None:
    WL2_TintColor_str=WL2_TintColor.split(",")
    WL2_TintR=WL2_TintColor_str[0]
    WL2_TintG=WL2_TintColor_str[1]
    WL2_TintB=WL2_TintColor_str[2]
else:
    WL2_TintR=None
    WL2_TintG=None
    WL2_TintB=None
try:
    WL3_TintColor=tint_color ('WearLayer3')
except KeyError:
    WL3_TintColor=None
    
if not WL3_TintColor ==None:
    WL3_TintColor_str=WL3_TintColor.split(",")
    WL3_TintR=WL3_TintColor_str[0]
    WL3_TintG=WL3_TintColor_str[1]
    WL3_TintB=WL3_TintColor_str[2]
else:
    WL3_TintR=None
    WL3_TintG=None
    WL3_TintB=None
try:
    WL4_TintColor=tint_color ('WearLayer4')
except KeyError:
    WL4_TintColor=None
    
if not WL4_TintColor ==None:
    WL4_TintColor_str=WL4_TintColor.split(",")
    WL4_TintR=WL4_TintColor_str[0]
    WL4_TintG=WL4_TintColor_str[1]
    WL4_TintB=WL4_TintColor_str[2]
else:
    WL4_TintR=None
    WL4_TintG=None
    WL4_TintB=None



ZExport=[
'mtlname="',(Base_Name or ""),'"\n',
#maps
'Base_TexMap_DDNA="',(Base_TexMap_DDNA or ""),'"\n',
'Base_TexMap_Decal="',(Base_TexMap_Decal or ""),'"\n',
'Base_TexMap_WDA="',(Base_TexMap_WDA or ""),'"\n',
'Base_TexMap_BLEND="',(Base_TexMap_DDNA or ""),'"\n',
#numbers

'Decal_SpecR="',(Decal_SpecR or ""),'"\n',
'Decal_SpecG="',(Decal_SpecG or ""),'"\n',
'Decal_SpecB="',(Decal_SpecB or ""),'"\n',

'DirtR="',(DirtR or ""),'"\n',
'DirtG="',(DirtG or ""),'"\n',
'DirtB="',(DirtB or ""),'"\n',

'Decal_Gloss="',(Decal_Gloss or ""),'"\n',


'BL1_TintR="',(DirtR or ""),'"\n',
'BL1_TintG="',(DirtG or ""),'"\n',
'BL1_TintB="',(DirtB or ""),'"\n',

'BL2_TintR="',(DirtR or ""),'"\n',
'BL2_TintG="',(DirtG or ""),'"\n',
'BL2_TintB="',(DirtB or ""),'"\n',

'BL3_TintR="',(DirtR or ""),'"\n',
'BL3_TintG="',(DirtG or ""),'"\n',
'BL3_TintB="',(DirtB or ""),'"\n',

'BL4_TintR="',(DirtR or ""),'"\n',
'BL4_TintG="',(DirtG or ""),'"\n',
'BL4_TintB="',(DirtB or ""),'"\n',

'WL1_TintR="',(DirtR or ""),'"\n',
'WL1_TintG="',(DirtG or ""),'"\n',
'WL1_TintB="',(DirtB or ""),'"\n',

'WL2_TintR="',(DirtR or ""),'"\n',
'WL2_TintG="',(DirtG or ""),'"\n',
'WL2_TintB="',(DirtB or ""),'"\n',

'WL3_TintR="',(DirtR or ""),'"\n',
'WL3_TintG="',(DirtG or ""),'"\n',
'WL3_TintB="',(DirtB or ""),'"\n',

'WL4_TintR="',(DirtR or ""),'"\n',
'WL4_TintG="',(DirtG or ""),'"\n',
'WL4_TintB="',(DirtB or ""),'"\n',

'BL1_Gloss="',(BL1_Gloss or ""),'"\n',
'BL2_Gloss="',(BL2_Gloss or ""),'"\n',
'BL3_Gloss="',(BL3_Gloss or ""),'"\n',
'BL4_Gloss="',(BL4_Gloss or ""),'"\n',

'WL1_Gloss="',(WL1_Gloss or ""),'"\n',
'WL2_Gloss="',(WL2_Gloss or ""),'"\n',
'WL3_Gloss="',(WL3_Gloss or ""),'"\n',
'WL4_Gloss="',(WL4_Gloss or ""),'"\n',

'BL1_UVTiling="',(BL1_UVTiling or ""),'"\n',
'BL2_UVTiling="',(BL2_UVTiling or ""),'"\n',
'BL3_UVTiling="',(BL3_UVTiling or ""),'"\n',
'BL4_UVTiling="',(BL4_UVTiling or ""),'"\n',

'WL1_UVTiling="',(WL1_UVTiling or ""),'"\n',
'WL2_UVTiling="',(WL2_UVTiling or ""),'"\n',
'WL3_UVTiling="',(WL3_UVTiling or ""),'"\n',
'WL4_UVTiling="',(WL4_UVTiling or ""),'"\n',


#paths
'BL1_Path="',(BL1_Path or ""),'"\n',
'BL2_Path="',(BL2_Path or ""),'"\n',
'BL3_Path="',(BL3_Path or ""),'"\n',
'BL4_Path="',(BL4_Path or ""),'"\n',

'WL1_Path="',(WL1_Path or ""),'"\n',
'WL2_Path="',(WL2_Path or ""),'"\n',
'WL3_Path="',(WL3_Path or ""),'"\n',
'WL4_Path="',(WL4_Path or ""),'"\n',

]

file1=open(cache_path + 'WCOutput.txt', 'w') #Set to your temp folder
file1.writelines(ZExport)
file1.close()



filematWC.close()

gc.collect()
