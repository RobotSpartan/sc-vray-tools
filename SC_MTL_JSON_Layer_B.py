import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 

maxINI = rt.getMAXIniFile()
tex_path = rt.getINISetting(maxINI, "SCTools", "tex_path")
cache_path = rt.getINISetting(maxINI, "SCTools", "cache_path")
script_path = rt.getINISetting(maxINI, "SCTools", "script_path")
json_path = rt.getINISetting(maxINI, "SCTools", "json_path")

filemat=open(cache_path + "zm2m.txt", 'r') #Set to your temp folder
matId=int(filemat.read())
filemat.close()
    
print(matId)


filemtl=open(cache_path + "zGetMtl.txt", 'r') #Set to your temp folder
Base_MTL_Load=str(filemtl.read())
filemtl.close()
Base_MTL_Load = Base_MTL_Load.replace("\n",'')
Base_MTL_Load = Base_MTL_Load.replace('\\', '/')
Base_MTL_Load = Base_MTL_Load.replace('"', '')

print(Base_MTL_Load)



SC_Base = json.load(open(Base_MTL_Load))


#Name
Base_Name=(SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Name'))

#Pull Spec information
Base_SpecularColor=(SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Specular'))
Base_SpecRGB_STR=Base_SpecularColor.split(",")
Base_SpecR=Base_SpecRGB_STR[0]
Base_SpecG=Base_SpecRGB_STR[1]
Base_SpecB=Base_SpecRGB_STR[2]



try:
    Base2_SpecularColor= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendLayer2SpecularColor')) 
except KeyError:
    Base2_SpecularColor=None
    
if not Base2_SpecularColor == None:    
    Base2_SpecRGB_STR=Base_SpecularColor.split(",")
    Base2_SpecR=Base_SpecRGB_STR[0]
    Base2_SpecG=Base_SpecRGB_STR[1]
    Base2_SpecB=Base_SpecRGB_STR[2]
else:
    Base2_SpecR=None
    Base2_SpecG=None
    Base2_SpecB=None
#Pull Diffuse

try:
    Base_DiffColor= (SC_Base  ['Material']['SubMaterials']['Material'][matId]['@Diffuse'])
except KeyError:
    Base_DiffColor=None

if not Base_DiffColor == None:
    Base_DiffRGB_STR=Base_DiffColor.split(",")
    Base_DiffR=Base_DiffRGB_STR[0]
    Base_DiffG=Base_DiffRGB_STR[1]
    Base_DiffB=Base_DiffRGB_STR[2]
else:
    Base_DiffR=None
    Base_DiffG=None
    Base_DiffB=None

try:
    Base2_DiffColor= (SC_Base  ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendLayer2DiffuseColor'))
except KeyError:
    Base2_DiffColor=None
    
if not Base2_DiffColor == None:
    Base2_DiffRGB_STR=Base_DiffColor.split(",")
    Base2_DiffR=Base_DiffRGB_STR[0]
    Base2_DiffG=Base_DiffRGB_STR[1]
    Base2_DiffB=Base_DiffRGB_STR[2]
else:
    Base2_DiffR=None
    Base2_DiffG=None
    Base2_DiffB=None

    #BlendMask
try:
    Base_BlendLayerTile=(SC_Base  ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendMaskTiling'))
except:
    Base_BlendLayerTile=None
try:
    Base_WearBlendBase=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@WearBlendBase'))
except:
   Base_WearBlendBase=None 
try:
    Base_BlendFactor=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendFactor'))
except:
    Base_BlendFactor=None
#Pull Gloss
try:
    Base_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Shininess'))
except:
    Base_Gloss=None
try:
    Base2_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendLayer2Glossiness'))
except:
    Base2_Gloss=None
    #Pull Detail Tile
try:
    Base_Tile=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@MacroTiling'))
except:
    Base_Tile=None
    #Detail Diffuse
try:
    Base_DetaiBase_Diff=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DetailDiffuseScale'))
except:
    Base_DetaiBase_Diff=None
try:
    BaseB_DetaiBase_Diff=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendDetailDiffuseScale'))
except:
    BaseB_DetaiBase_Diff=None    
    #Detail Normal
try:
    Base_Detail_Nrm=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendDetailBumpScale'))
except:
    Base_Detail_Nrm=None
try:
    BaseB_Detail_Nrm=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DetailBump'))
except:
    BaseB_Detail_Nrm=None
#Detail Gloss
try:
    Base_Detail_Gloss=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('DetailGlossScale'))
except:
    Base_Detail_Gloss=None
try:
    BaseB_Detail_Gloss=(SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@BlendDetailGlossScale'))
except:
    BaseB_Detail_Gloss=None



#baseblendLayer
def search_texmap (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']

# MatLayer
def search_layer (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@Path']

   
def tint_layer (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@TintColor']

   
def tint_GlossMult (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@GlossMult']

   
def tint_UVtile (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@UVTiling']


try:
    Base_TextMapDiff=search_texmap ('TexSlot1')
except KeyError:
    Base_TextMapDiff=None
    
try:
    Base_TextMapNormal=search_texmap ('TexSlot2')
except KeyError:
    Base_TextMapNormal=None
    
try:
    Base_TextMapDetail=search_texmap ('TexSlot6')
except KeyError:
    Base_TextMapDetail=None
try:
    Base_TextMapSpec=search_texmap ('TexSlot4')
except KeyError:
    Base_TextMapSpec=None
    
try:
    BaseB_TextMapDiff=search_texmap ('TexSlot9')
except KeyError:
    BaseB_TextMapDiff=None
try:
    Base_TextMapDispl=search_texmap ('TexSlot8')
except KeyError:
    Base_TextMapDispl=None
try:
    BaseB_TextMapNormal=search_texmap ('TexSlot3')
except KeyError:
    BaseB_TextMapNormal=None
try:
    BaseB_TextMapDetail=search_texmap ('TexSlot13')
except KeyError:
    BaseB_TextMapDetail=None
try:
    BaseB_TextMapSpec=search_texmap ('TexSlot10')
except KeyError:
    BaseB_TextMapSpec=None
try:
    BaseB_TextMapMask=search_texmap ('TexSlot12')
except KeyError:
    BaseB_TextMapMask=None


try:
    Layer1mtl=search_layer ('Primary')
    Layer2mtl=search_layer ('Wear')
except KeyError:
    Layer1mtl=None
    Layer2mtl=None

#Layer 1 information
try:
    Base_TintColor1=tint_layer ('Primary')
except KeyError:
    Base_TintColor1=None
    
if not Base_TintColor1== None:
    Base_TintColor1_str=Base_TintColor1.split(",")
    Base_TintR=Base_TintColor1_str[0]
    Base_TintG=Base_TintColor1_str[1]
    Base_TintB=Base_TintColor1_str[2]
else:
    Base_TintR=None
    Base_TintG=None
    Base_TintB=None

#Layer 2 information
try:
    Base_TintColor2=tint_layer ('Wear')
except KeyError:
    Base_TintColor2=None
    
if not Base_TintColor2 ==None:
    Base_TintColor2_str=Base_TintColor2.split(",")
    Base2_TintR=Base_TintColor2_str[0]
    Base2_TintG=Base_TintColor2_str[1]
    Base2_TintB=Base_TintColor2_str[2]
else:
    Base2_TintR=None
    Base2_TintG=None
    Base2_TintB=None

try: 
    #Layer 1 Gloss
    Layer1Gloss=tint_GlossMult ('Primary')
    #Layer 2 Gloss
    Layer2Gloss=tint_GlossMult ('Wear')

    #Layer 1 UV Tile
    Layer1UV=tint_UVtile ('Primary')
    #Layer 1 UV Tile
    Layer2UV=tint_UVtile ('Wear')
except KeyError:
    Layer1Gloss=None
    Layer2Gloss=None
    Layer1UV=None
    Layer2UV=None


ZExport=[
'mtlname="',(Base_Name or ""),'"\n',
'Base_diff="',(Base_TextMapDiff or ""),'"\n',
'Base_ddna="',(Base_TextMapNormal or ""),'"\n',
'Base_detail="',(Base_TextMapDetail or ""),'"\n',
'Base_spec="',(Base_TextMapSpec or ""),'"\n',
'BaseB_diff="',(BaseB_TextMapDiff or ""),'"\n',
'Base_disp="',(Base_TextMapDispl or ""),'"\n',
'BaseB_ddna="',(BaseB_TextMapNormal or ""),'"\n',
'BaseB_detail="',(BaseB_TextMapDetail or ""),'"\n',
'BaseB_spec="',(BaseB_TextMapSpec or ""),'"\n',
'Base_mask="',(BaseB_TextMapMask or ""),'"\n',
'Base_SpecR="',(Base_SpecR or ""),'"\n',
'Base_SpecG="',(Base_SpecG or ""),'"\n',
'Base_SpecB="',(Base_SpecB or ""),'"\n',
'BaseB_SpecR="',(Base2_SpecR or ""),'"\n',
'BaseB_SpecG="',(Base2_SpecG or ""),'"\n',
'BaseB_SpecB="',(Base2_SpecB or ""),'"\n',
'Base_DiffR="',(Base_DiffR or ""),'"\n',
'Base_DiffG="',(Base_DiffG or ""),'"\n',
'Base_DiffB="',(Base_DiffB or ""),'"\n',
'Base2_DiffR="',(Base2_DiffR or ""),'"\n',
'Base2_DiffG="',(Base2_DiffG or ""),'"\n',
'Base2_DiffB="',(Base2_DiffB or ""),'"\n',
'Base_BlendLayerTile="',(Base_BlendLayerTile or ""),'"\n',
'Base_WearBlendBase="',(Base_WearBlendBase or ""),'"\n',
'Base_BlendFactor="',(Base_BlendFactor or ""),'"\n',
'Base_Gloss="',(Base_Gloss or ""),'"\n',
'Base2_Gloss="',(Base2_Gloss or ""),'"\n',
'Base_Tile="',(Base_Tile or ""),'"\n',
'Base_Detail_DiffScale="',(Base_DetaiBase_Diff or ""),'"\n',
'Base2_Detail_DiffScale="',(BaseB_DetaiBase_Diff or ""),'"\n',
'Base_Detail_Nrm="',(Base_Detail_Nrm or ""),'"\n',
'BaseB_Detail_Nrm="',(BaseB_Detail_Nrm or ""),'"\n',
'Base_Detail_Gloss="',(Base_Detail_Gloss or ""),'"\n',
'BaseB_Detail_Gloss="',(BaseB_Detail_Gloss or ""),'"\n',
'Base_TintR="',(Base_TintR or ""),'"\n',
'Base_TintG="',(Base_TintG or ""),'"\n',
'Base_TintB="',(Base_TintB or ""),'"\n',
'Base2_TintR="',(Base2_TintR or ""),'"\n',
'Base2_TintG="',(Base2_TintG or ""),'"\n',
'Base2_TintB="',(Base2_TintB or ""),'"\n',
'Base_LayerGloss="',(Layer1Gloss or ""),'"\n',
'Base2_LayerGloss="',(Layer2Gloss or ""),'"\n',
'Base_Layer1UV="',(Layer1UV or "1"),'"\n',
'Base2_Layer2UV="',(Layer2UV or "1"),'"\n',
'LayerTF2="',(Layer2mtl or ""),'"\n',
'LayerTF1="',(Layer1mtl or ""),'"\n',
]


Layerz_Input=[Layer1mtl or ""]

LayerX_Input=[Layer2mtl or ""]


file1=open(cache_path + 'BaseOutput.txt', 'w')#Set to your temp folder
file1.writelines(ZExport)
file1.close()

file2=open(cache_path + 'Layer1Input.txt', 'w')#Set to your temp folder
file2.writelines(Layerz_Input)
file2.close()

file3=open(cache_path + 'Layer2Input.txt', 'w')#Set to your temp folder
file3.writelines(LayerX_Input)
file3.close()

filemtl.close()

gc.collect()
