try (closeRolloutFloater SCLightLoadVray) catch()

maxINI = getMAXIniFile()
tex_path=D:/SC_Data_Mining/StarFab_Exports/textures
cache_path=D:\SC_Data_Mining\StarFab_Exports\SC_Cache
script_path=C:/Program Files/Autodesk/3ds Max 2022/scripts/sc-vray-tools-main
json_path=D:/SC_Data_Mining/StarFab_Exports/json/Data
rollout SCLightLoadVray "SC Light Loader Vray alpha 0.0.2" width:600 height:485
(
	bitmap 'bmp1' "Bitmap" pos:[0,0] width:600 height:100 align:#left filename: "StarFabPython/VrayLightImporter.tiff"
	editText 'FilePath' "Save Path" pos:[46,122] width:505 height:20 align:#left text:"T:/light/"--ChangePath_here
	button 'btn_Light_STD' "Default Mode" pos:[25,199] width:143 height:53 align:#left
	button 'btn_Light_AUX' "Auxiliary Mode" pos:[230,199] width:143 height:53 align:#left
	button 'btn_Light_EMG' "Emergency Mode" pos:[435,199] width:143 height:53 align:#left
	button 'btn_save_std' "Save as Default" pos:[28,293] width:143 height:21 align:#left
	button 'btn_save_aux' "Save as Aux" pos:[230,293] width:143 height:21 align:#left
	button 'btn_save_emg' "Save As Emergency" pos:[433,293] width:143 height:21 align:#left
	bitmap 'bmp2' "Bitmap" pos:[27,360] width:80 height:80 align:#left filename: "StarFabPython/Logo_Community_logo.tiff"
	groupBox 'grp1' "Instruction" pos:[118,351] width:463 height:130 align:#left
	label 'lbl1' "Lights that are loaded in are missing their transforms (Position and Rotation) this is due to maxscript having issues reading the data directly in the scbp. Close and Reopen to load another scbp" pos:[126,366] width:450 height:110 align:#left
	
	on btn_Light_STD pressed  do
	(
		python.ExecuteFile script_path + "\StarFab_MAX_LightMaker_02_default_VRAY.py" --change to your script dir
		

	)
	on btn_Light_EMG pressed  do
	(
		python.ExecuteFile script_path + "\StarFab_MAX_LightMaker_02_Emergency_VRAY.py" --change to your script dir
 
	)
	on btn_Light_AUX pressed  do
	(
		python.ExecuteFile script_path + "\StarFab_MAX_LightMaker_02_Auxilary_VRAY.py" --change to your script dir

	)
	on btn_save_std pressed do
	(
		saveMaxFile (FilePath.text+"Import_Light_STD.max")
	)
	on btn_save_aux pressed do
	(
		saveMaxFile (FilePath.text+"Import_Light_AUX.max")
	)
	on btn_save_emg pressed do
	(
		saveMaxFile (FilePath.text+"Import_Light_EMG.max")
	)

)
CreateDialog SCLightLoadVray
