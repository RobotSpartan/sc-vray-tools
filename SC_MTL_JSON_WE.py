import json
import pymxs #MaxScript Wrapper  
from pymxs import runtime as rt 

maxINI = rt.getMAXIniFile()
tex_path = rt.getINISetting(maxINI, "SCTools", "tex_path")
cache_path = rt.getINISetting(maxINI, "SCTools", "cache_path")
script_path = rt.getINISetting(maxINI, "SCTools", "script_path")
json_path = rt.getINISetting(maxINI, "SCTools", "json_path")

filematWE=open(cache_path + "\WE2m.txt", 'r') #Set to your temp folder
matId=int(filematWE.read())
filematWE.close()
    
print(matId)


filemtl=open(cache_path + "\WEGetMtl.txt", 'r') #Set to your temp folder
Base_MTL_Load=str(filemtl.read())
filemtl.close()
Base_MTL_Load = Base_MTL_Load.replace("\n",'')
Base_MTL_Load = Base_MTL_Load.replace('\\', '/')
Base_MTL_Load = Base_MTL_Load.replace('"', '')

print(Base_MTL_Load)



SC_Base = json.load(open(Base_MTL_Load))


#Name
Base_Name=(SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Name'))


#Pull Parameters
try:
    MasterGloss= (SC_Base ['Material']['SubMaterials']['Material'][matId].get('@Shininess'))
except KeyError:
    MasterGloss=None
    
try:
    Dirt_Color= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DirtColor')) 
except KeyError:
    Dirt_Color=None
    
if not Dirt_Color == None:    
    DirtRGB_STR=Dirt_Color.split(",")
    DirtR=DirtRGB_STR[0]
    DirtG=DirtRGB_STR[1]
    DirtB=DirtRGB_STR[2]
else:
    DirtR=None
    DirtG=None
    DirtB=None
    
try:
    Decal_Gloss= (SC_Base ['Material']['SubMaterials']['Material'][matId]['PublicParams'].get('@DiffuseDecalGloss')) 
except KeyError:
    Decal_Gloss=None


    #BlendMask



#baseblendLayer
def search_texmap (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['Textures']['Texture']):
  if name.lower() == keyval['@Map'].lower():
   return keyval['@File']

# MatLayer


def tint_path (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@Path']

def tint_color (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@TintColor']
   
def tint_GlossMult (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@GlossMult']

def tint_UVtiling (name):
 for keyval in (SC_Base ['Material']['SubMaterials']['Material'][matId]['MatLayers']['Layer']):
  if name.lower() == keyval['@Name'].lower():
   return keyval['@UVTiling']


#Textmaps
try:
    Base_TexMap_DDNA=search_texmap ('TexSlot3')
except KeyError:
    Base_TexMap_DDNA=None
    

try:
    Base_TexMap_WDA=search_texmap ('TexSlot11')
except KeyError:
    Base_TexMap_WDA=None
    
#LayerPaths
try:
    BL1_Path= tint_path ("BaseLayer1")
except KeyError:
    BL1_Path=None

try:
    WL1_Path= tint_path ("WearLayer1")
except KeyError:
    WL1_Path=None



#LayerGloss
try:
    BL1_Gloss= tint_GlossMult ("BaseLayer1")
except KeyError:
    BL1_Gloss=None

try:
    WL1_Gloss= tint_GlossMult ("WearLayer1")
except KeyError:
    WL1_Gloss=None


#LayerUV
try:
    BL1_UVTiling= tint_UVtiling ("BaseLayer1")
except KeyError:
    BL1_UVTiling=None

try:
    WL1_UVTiling= tint_UVtiling ("WearLayer1")
except KeyError:
    WL1_UVTiling=None



#LayerTintColor

try:
    BL1_TintColor=tint_color ('BaseLayer1')
except KeyError:
    BL1_TintColor=None
    
if not BL1_TintColor ==None:
    BL1_TintColor_str=BL1_TintColor.split(",")
    BL1_TintR=BL1_TintColor_str[0]
    BL1_TintG=BL1_TintColor_str[1]
    BL1_TintB=BL1_TintColor_str[2]
else:
    BL1_TintR=None
    BL1_TintG=None
    BL1_TintB=None
    
    
try:
    WL1_TintColor=tint_color ('WearLayer1')
except KeyError:
    WL1_TintColor=None
    
if not WL1_TintColor ==None:
    WL1_TintColor_str=WL1_TintColor.split(",")
    WL1_TintR=WL1_TintColor_str[0]
    WL1_TintG=WL1_TintColor_str[1]
    WL1_TintB=WL1_TintColor_str[2]
else:
    WL1_TintR=None
    WL1_TintG=None
    WL1_TintB=None




EExport=[
'mtlname="',(Base_Name or ""),'"\n',
#maps
'Base_TexMap_DDNA="',(Base_TexMap_DDNA or ""),'"\n',
'Base_TexMap_WDA="',(Base_TexMap_WDA or ""),'"\n',
#numbers

#Master Scale
'MasterGloss="',(MasterGloss or ""),'"\n',
#Dirt
'DirtR="',(DirtR or ""),'"\n',
'DirtG="',(DirtG or ""),'"\n',
'DirtB="',(DirtB or ""),'"\n',

'BL1_TintR="',(BL1_TintR or ""),'"\n',
'BL1_TintG="',(BL1_TintG or ""),'"\n',
'BL1_TintB="',(BL1_TintB or ""),'"\n',

'WL1_TintR="',(WL1_TintR or ""),'"\n',
'WL1_TintG="',(WL1_TintG or ""),'"\n',
'WL1_TintB="',(WL1_TintB or ""),'"\n',

#Gloss
'Decal_Gloss="',(Decal_Gloss or ""),'"\n',
'BL1_Gloss="',(BL1_Gloss or ""),'"\n',
'WL1_Gloss="',(WL1_Gloss or ""),'"\n',

#tile
'BL1_UVTiling="',(BL1_UVTiling or ""),'"\n',
'WL1_UVTiling="',(WL1_UVTiling or ""),'"\n',





#paths
'BL1_Path="',(BL1_Path or "blank"),'"\n',
'WL1_Path="',(WL1_Path or "blank"),'"\n',


]

fileWE=open(cache_path + 'WEOutput.txt', 'w') #Set to your temp folder
fileWE.writelines(EExport)
fileWE.close()



filematWE.close()

gc.collect()
